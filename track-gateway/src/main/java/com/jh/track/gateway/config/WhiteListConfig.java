package com.jh.track.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 17:57
 * @description：白名单配置
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "whitelist")
public class WhiteListConfig {

    private List<String> urls;

}
