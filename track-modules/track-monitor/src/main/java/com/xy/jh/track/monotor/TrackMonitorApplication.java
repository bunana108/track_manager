package com.xy.jh.track.monotor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author ：xy
 * @date ：Created in 2021/3/2 14:35
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@SpringBootApplication
@EnableAdminServer
public class TrackMonitorApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(TrackMonitorApplication.class, args);

    }
}
