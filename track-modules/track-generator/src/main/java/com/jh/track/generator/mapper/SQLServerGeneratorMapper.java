package com.jh.track.generator.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * SQLServer代码生成器
 *
 */
@Mapper
public interface SQLServerGeneratorMapper extends GeneratorMapper {

}
