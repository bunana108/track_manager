package com.jh.track.generator.mapper;

import org.apache.ibatis.annotations.Mapper;

/**
 * PostgreSQL代码生成器
 *

 */
@Mapper
public interface PostgreSQLGeneratorMapper extends GeneratorMapper {

}
