

package com.jh.track.generator.config;


import com.jh.track.generator.mapper.*;
import com.jh.track.common.core.exception.TrackException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * 数据库配置
 *
 */
@Configuration
public class DbConfig {
    @Value("${track.database: mysql}")
    private String database;
    @Autowired
    private MySQLGeneratorMapper mySQLGeneratorDao;
    @Autowired
    private OracleGeneratorMapper oracleGeneratorDao;
    @Autowired
    private SQLServerGeneratorMapper sqlServerGeneratorDao;
    @Autowired
    private PostgreSQLGeneratorMapper postgreSQLGeneratorDao;

    @Bean
    @Primary
    public GeneratorMapper getGeneratorDao(){
        if("mysql".equalsIgnoreCase(database)){
            return mySQLGeneratorDao;
        }else if("oracle".equalsIgnoreCase(database)){
            return oracleGeneratorDao;
        }else if("sqlserver".equalsIgnoreCase(database)){
            return sqlServerGeneratorDao;
        }else if("postgresql".equalsIgnoreCase(database)){
            return postgreSQLGeneratorDao;
        }else {
            throw new TrackException("不支持当前数据库：" + database);
        }
    }
}
