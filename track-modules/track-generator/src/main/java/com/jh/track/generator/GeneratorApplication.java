package com.jh.track.generator;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ：xy
 * @date ：Created in 2021/4/23 10:53
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@SpringBootApplication
@MapperScan
public class GeneratorApplication {
    public static void main(String[] args) {
        SpringApplication.run(GeneratorApplication.class,args);
    }
}
