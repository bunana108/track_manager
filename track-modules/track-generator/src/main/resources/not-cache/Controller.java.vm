package ${package}.${moduleName}.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import ${package}.${moduleName}.pojo.entity.${className};
import ${package}.${moduleName}.service.${className}Service;
import ${mainPath}.common.core.utils.PageUtils;
import ${mainPath}.common.core.result.Result;
import ${mainPath}.common.core.base.BaseController;


/**
 * ${comments}
 *
 * @author ${author}
 * @email ${email}
 * @date ${datetime}
 */
@RestController
@RequestMapping("/api.${moduleName}/v1/${classname}")
@Api(value = "$!{comments}", tags = "$!{comments}接口")
public class ${className}Controller extends BaseController {
    @Autowired
    private ${className}Service ${classname}Service;

    /**
    * 分页${comments}列表}
    *
    * @param params 　
    * @return Result
    */
    @GetMapping("/page")
    @ApiOperation(value = "$!{comments}列表", notes = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currPage", required = true, value = "当前页", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", required = true, value = "每页显示数据", paramType = "form"),
            @ApiImplicitParam(name = "totalPage", required = true, value = "总页数", paramType = "form"),
            @ApiImplicitParam(name = "totalCount", required = true, value = "总记录数", paramType = "form"),
    })

    public Result list(@RequestParam Map<String, Object> params){
        PageUtils page = ${classname}Service.queryPage(params);

        return Result.success(page);
    }

    /**
     * ${comments}详情
     */
    @GetMapping("/{${pk.attrname}}")
    @ApiOperation(value = "${comments}详情")
    @ApiImplicitParam(name = "{${pk.attrname}", value = "{${pk.comments}", required = true, paramType = "path", dataType = "${pk.attrType}")
    public Result detail(@PathVariable("${pk.attrname}") ${pk.attrType} ${pk.attrname}){
		${className} ${classname} = ${classname}Service.getById(${pk.attrname});

        return Result.success(${classname});

    }

    /**
     * ${comments}新增
     */
    @PostMapping
    @ApiOperation(value = "新增${comments}")
    @ApiImplicitParam(name = "${comments}", value = "实体JSON对象", required = true, paramType = "body", dataType = "${className}")
    public Result add(@RequestBody ${className} ${classname}){
		boolean state = ${classname}Service.save(${classname});

        return Result.judge(state);
    }

    /**
     * 修改${comments}
     */
    @PutMapping(value = "/{id}")
    @ApiOperation(value = "修改菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "${pk.attrname}", value = "${comments}id}", required = true, paramType = "path", dataType = "${pk.attrType}"),
            @ApiImplicitParam(name = "${classname}", value = "实体JSON对象", required = true, paramType = "body", dataType = "${className}")
    })
    public Result update(@RequestBody ${className} ${classname}){
        boolean status =	${classname}Service.updateById(${classname});

        return Result.judge(status);
    }

    /**
     * $!{pk.comments}删除
     *
     * @param ${pk.attrname}s id字符串，根据,号分隔
     * @return Result
     */
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除$!{comments}", notes = "删除$!{comments}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "id集合字符串，以,分割", required = true, paramType = "query", dataType = "String")
    })
    public Result delete(@PathVariable("ids") String ids){
		boolean status =  ${classname}Service.removeByIds(Arrays.asList(ids.split(",")));

        return Result.judge(status);
    }

}
