package com.jh.track.mill.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jh.track.common.core.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
/**
 * 加热炉跟踪表
 * 
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@ApiModel("加热炉跟踪表的实体")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("track_rhf")
public class Rhf extends BaseEntity{
	private static final long serialVersionUID = 1L;
/**
 * 物料号
 */
	@TableId(type = IdType.INPUT)
	@ApiModelProperty(value = " 物料号")
	private Long matNo;
/**
 * 轧制号
 */
	@ApiModelProperty(value = " 轧制号")
	private String lotNo;
/**
 * 批内序号
 */
	@ApiModelProperty(value = " 批内序号")
	private Integer serialNumber;
/**
 * 钢坯号
 */
	@ApiModelProperty(value = " 钢坯号")
	private String billetId;
/**
 * 炉号
 */
	@ApiModelProperty(value = " 炉号")
	private String heatNo;
/**
 * 厚度
 */
	@ApiModelProperty(value = " 厚度")
	private Float thick;
/**
 * 宽度
 */
	@ApiModelProperty(value = " 宽度")
	private Float width;
/**
 * 长度
 */
	@ApiModelProperty(value = " 长度")
	private Float length;
/**
 * 目标直径
 */
	@ApiModelProperty(value = " 目标直径")
	private Float orderDiameter;
/**
 * 加热炉号
 */
	@ApiModelProperty(value = " 加热炉号")
	private Integer furNo;
/**
 * 加热炉行号
 */
	@ApiModelProperty(value = " 加热炉行号")
	private Integer rowNo;
/**
 * 坐标
 */
	@ApiModelProperty(value = " 坐标")
	private BigDecimal coordinate;
/**
 * 入炉时间
 */
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = " 入炉时间")
	private Date chargeDate;
/**
 * 钢种
 */
	@ApiModelProperty(value = " 钢种")
	private String tradeNo;
/**
 * 计划号
 */
	@ApiModelProperty(value = " 计划号")
	private Long pdiId;

}
