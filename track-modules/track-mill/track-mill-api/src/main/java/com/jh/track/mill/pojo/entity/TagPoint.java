package com.jh.track.mill.pojo.entity;

import lombok.Data;

/**
 * @author ：xy
 * @date ：Created in 2021/4/21 16:10
 * @description： PLC 采集点
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
public class TagPoint {
    private int pointId;
    private String valueType;
    private String value;
}
