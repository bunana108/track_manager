package com.jh.track.mill.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jh.track.common.core.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
/**
 * 事件跟踪表
 * 
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@ApiModel("事件跟踪表的实体")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("track_event")
public class Event extends BaseEntity{
	private static final long serialVersionUID = 1L;
/**
 * 主键
 */
	@TableId(type = IdType.INPUT)
	@ApiModelProperty(value = " 主键")
	private Long id;
/**
 * 事件号
 */
	@ApiModelProperty(value = " 事件号")
	private Integer eventId;
/**
 * 事件类型
 */
	@ApiModelProperty(value = " 事件类型")
	private String eventType;
/**
 * 事件名
 */
	@ApiModelProperty(value = " 事件名")
	private String eventName;
/**
 * 事件取设备名
 */
	@ApiModelProperty(value = " 事件取设备名")
	private String eventEquipment;
/**
 * 起始位置
 */
	@ApiModelProperty(value = " 起始位置")
	private Integer positionFrom;
/**
 * 目标位置
 */
	@ApiModelProperty(value = " 目标位置")
	private Integer positionTo;
/**
 * 事件方向
 */
	@ApiModelProperty(value = " 事件方向")
	private String eventDirection;
/**
 * 使用标记
 */
	@ApiModelProperty(value = " 使用标记")
	private String useFlag;

}
