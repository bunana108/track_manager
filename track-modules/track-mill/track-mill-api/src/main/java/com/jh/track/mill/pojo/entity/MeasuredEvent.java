package com.jh.track.mill.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jh.track.common.core.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
/**
 * 
 * 
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:13:49
 */
@ApiModel("的实体")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("track_measured_event")
public class MeasuredEvent extends BaseEntity{
	private static final long serialVersionUID = 1L;
/**
 * 
 */
	@TableId(type = IdType.INPUT)
	@ApiModelProperty(value = " ")
	private Long id;

}
