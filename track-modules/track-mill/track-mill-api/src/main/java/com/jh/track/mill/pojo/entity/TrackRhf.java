package com.jh.track.mill.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.jh.track.common.core.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author ：xy
 * @date ：Created in 2021/4/20 21:50
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel("加热炉跟踪实体")
public class TrackRhf extends BaseEntity {
    @ApiModelProperty(value = "主键Id")
    @TableId(type = IdType.INPUT)
    private Long id;
    @ApiModelProperty(value = "物料号")
    private Long matNo;
    @ApiModelProperty(value = "轧制号")
    private String lotNo;
    @ApiModelProperty(value = "批内序号")
    private Integer serialNumber;
    @ApiModelProperty(value = "钢坯号")
    private String billetId;
    @ApiModelProperty(value = "炉号")
    private String heatNo;
    @ApiModelProperty(value = "厚度")
    private Float thick;
    @ApiModelProperty(value = "宽度")
    private Float width;
    @ApiModelProperty(value = "长度")
    private Float length;
    @ApiModelProperty(value = "目标直径")
    private Float orderDiameter;
    @ApiModelProperty(value = "加热炉号")
    private Integer furNo;
    @ApiModelProperty(value = "加热炉内行号")
    private Integer rowNo;
    @ApiModelProperty(value = "坐标")
    private Float coordinate;
    @ApiModelProperty(value = "入炉时间")
    private Date chargeDate;
    @ApiModelProperty(value = "钢种")
    private String tradeNo;
    @ApiModelProperty(value = "计划号")
    private Long pdiId;


}
