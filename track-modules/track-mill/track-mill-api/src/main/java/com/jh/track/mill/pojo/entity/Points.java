package com.jh.track.mill.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jh.track.common.core.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
/**
 * 跟踪点位表
 * 
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@ApiModel("跟踪点位表的实体")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("track_points")
public class Points extends BaseEntity{
	private static final long serialVersionUID = 1L;
/**
 * 主键
 */
	@TableId(type = IdType.INPUT)
	@ApiModelProperty(value = " 主键")
	private Long id;
/**
 * 点位号
 */
	@ApiModelProperty(value = " 点位号")
	private Integer pointId;
/**
 * 点位类型
 */
	@ApiModelProperty(value = " 点位类型")
	private String pointType;
/**
 * 点位名
 */
	@ApiModelProperty(value = " 点位名")
	private String pointName;
/**
 * 点位值
 */
	@ApiModelProperty(value = " 点位值")
	private String pointValue;
/**
 * 事件号
 */
	@ApiModelProperty(value = " 事件号")
	private Integer eventId;
/**
 * 设备编号
 */
	@ApiModelProperty(value = " 设备编号")
	private Integer deviceId;
/**
 * 使用标志
 */
	@ApiModelProperty(value = " 使用标志")
	private String useFlag;

}
