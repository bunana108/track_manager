package com.jh.track.mill.pojo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author ：xy
 * @date ：Created in 2021/4/21 15:20
 * @description： PLC采集点组
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
public class TagGroup implements Serializable {
    private final static long serialVersionUID = 1L;
    @JsonProperty("ChannelName")
    private String channelName;
    @JsonProperty("TagGroupName")
    private String tagGroupName;
    @JsonProperty("TagResultList")
    private List<Tag> tagResultList;
    @JsonProperty("TagGroupResult")
    private String tagGroupResult;
    @JsonProperty("Time")
    private String time;
}