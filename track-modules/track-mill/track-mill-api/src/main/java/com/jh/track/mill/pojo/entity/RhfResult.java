package com.jh.track.mill.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jh.track.common.core.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
/**
 * 
 * 
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@ApiModel("的实体")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("rhf_result")
public class RhfResult extends BaseEntity{
	private static final long serialVersionUID = 1L;
/**
 * 主键
 */
	@TableId(type = IdType.INPUT)
	@ApiModelProperty(value = " 主键")
	private Long id;
/**
 * 物料号
 */
	@ApiModelProperty(value = " 物料号")
	private Long matNo;
/**
 * 轧制号
 */
	@ApiModelProperty(value = " 轧制号")
	private String lotNo;
/**
 * 钢坯序号
 */
	@ApiModelProperty(value = " 钢坯序号")
	private Integer serialNumber;
/**
 * 炉号
 */
	@ApiModelProperty(value = " 炉号")
	private String heatNo;
/**
 * 钢坯号
 */
	@ApiModelProperty(value = " 钢坯号")
	private String billetId;
/**
 * 附加炉号
 */
	@ApiModelProperty(value = " 附加炉号")
	private String recode;
/**
 * 牌号
 */
	@ApiModelProperty(value = " 牌号")
	private String tradeNo;
/**
 * 目标直径
 */
	@ApiModelProperty(value = " 目标直径")
	private Float orderDiameter;
/**
 * 钢种
 */
	@ApiModelProperty(value = " 钢种")
	private String spec;
/**
 * 入炉温度
 */
	@ApiModelProperty(value = " 入炉温度")
	private Float inRhfTemp;
/**
 * 钢坯入炉时间
 */
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = " 钢坯入炉时间")
	private Date inHeatDate;
/**
 * 预热段热电偶1
 */
	@ApiModelProperty(value = " 预热段热电偶1")
	private Float preheatingTemp1;
/**
 * 预热段热电偶2
 */
	@ApiModelProperty(value = " 预热段热电偶2")
	private Float preheatingTemp2;
/**
 * 预热段热电偶3
 */
	@ApiModelProperty(value = " 预热段热电偶3")
	private Float preheatingTemp3;
/**
 * 预热段上热电偶
 */
	@ApiModelProperty(value = " 预热段上热电偶")
	private Float preheatingUpTemp;
/**
 * 预热段下热电偶
 */
	@ApiModelProperty(value = " 预热段下热电偶")
	private Float preheatingDownTemp;
/**
 * 预热段加热实绩
 */
	@ApiModelProperty(value = " 预热段加热实绩")
	private Integer preheatingTime;
/**
 * 进加热段时间
 */
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = " 进加热段时间")
	private Date inHeatingDate;
/**
 * 加热段热电偶1
 */
	@ApiModelProperty(value = " 加热段热电偶1")
	private Float heatingTemp1;
/**
 * 加热段热电偶2
 */
	@ApiModelProperty(value = " 加热段热电偶2")
	private Float heatingTemp2;
/**
 * 加热段热电偶3
 */
	@ApiModelProperty(value = " 加热段热电偶3")
	private Float heatingTemp3;
/**
 * 加热段热电偶4
 */
	@ApiModelProperty(value = " 加热段热电偶4")
	private Float heatingTemp4;
/**
 * 加热段上热电偶
 */
	@ApiModelProperty(value = " 加热段上热电偶")
	private Float heatingUpTemp;
/**
 * 加热段下热电偶
 */
	@ApiModelProperty(value = " 加热段下热电偶")
	private Float heatingDownTemp;
/**
 * 进均热段时间
 */
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = " 进均热段时间")
	private Date inSoakingDate;
/**
 * 加热段加热时间
 */
	@ApiModelProperty(value = " 加热段加热时间")
	private Integer heatingTime;
/**
 * 均热段热电偶1
 */
	@ApiModelProperty(value = " 均热段热电偶1")
	private Float soakingTemp1;
/**
 * 均热段热电偶2
 */
	@ApiModelProperty(value = " 均热段热电偶2")
	private Float soakingTemp2;
/**
 * 均热段上热电偶
 */
	@ApiModelProperty(value = " 均热段上热电偶")
	private Float soakingUpTemp;
/**
 * 均热段下热电偶
 */
	@ApiModelProperty(value = " 均热段下热电偶")
	private Float soakingDownTemp;
/**
 * 均热段加热时间
 */
	@ApiModelProperty(value = " 均热段加热时间")
	private Integer soakingTime;
/**
 * 出炉时间
 */
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = " 出炉时间")
	private Date outHeatDate;
/**
 * 出炉间隔时间
 */
	@ApiModelProperty(value = " 出炉间隔时间")
	private Integer rhfThm;
/**
 * 均热段煤气侧平均温度
 */
	@ApiModelProperty(value = " 均热段煤气侧平均温度")
	private Float avgGasTemp;
/**
 * 均热的空气侧平均温度
 */
	@ApiModelProperty(value = " 均热的空气侧平均温度")
	private Float avgAirTemp;
/**
 * 加热二段煤气侧平均温度
 */
	@ApiModelProperty(value = " 加热二段煤气侧平均温度")
	private Float warmGasTemp2;
/**
 * 加热二段空气侧平均温度
 */
	@ApiModelProperty(value = " 加热二段空气侧平均温度")
	private Float warmAirTemp2;
/**
 * 加热一段煤气侧平均温度
 */
	@ApiModelProperty(value = " 加热一段煤气侧平均温度")
	private Float warmGasTemp1;
/**
 * 加热一段空气侧平均温度
 */
	@ApiModelProperty(value = " 加热一段空气侧平均温度")
	private Float warmAirTemp1;
/**
 * 排烟温度
 */
	@ApiModelProperty(value = " 排烟温度")
	private Float avgSmokeTemp;
/**
 * 均热段煤气流量
 */
	@ApiModelProperty(value = " 均热段煤气流量")
	private Float avgGasFlow;
/**
 * 均热段空气流量
 */
	@ApiModelProperty(value = " 均热段空气流量")
	private Float avgAirFlow;
/**
 * 加热二段煤气流量
 */
	@ApiModelProperty(value = " 加热二段煤气流量")
	private Float warmGasFlow2;
/**
 * 加热二段空气流量
 */
	@ApiModelProperty(value = " 加热二段空气流量")
	private Float warmAirFlow2;
/**
 * 加热一段煤气流量
 */
	@ApiModelProperty(value = " 加热一段煤气流量")
	private Float warmGasFlow1;
/**
 * 加热二段空气流量
 */
	@ApiModelProperty(value = " 加热二段空气流量")
	private Float warmAirFlow1;
/**
 * 炉体1#CO含量
 */
	@ApiModelProperty(value = " 炉体1#CO含量")
	private Float co1;
/**
 * 炉体2#CO含量
 */
	@ApiModelProperty(value = " 炉体2#CO含量")
	private Float co2;
/**
 * 炉体3#CO含量
 */
	@ApiModelProperty(value = " 炉体3#CO含量")
	private Float co3;
/**
 * 炉体4#CO含量
 */
	@ApiModelProperty(value = " 炉体4#CO含量")
	private Float co4;
/**
 * 炉体5#CO含量
 */
	@ApiModelProperty(value = " 炉体5#CO含量")
	private Float co5;
/**
 * 炉体6#CO含量
 */
	@ApiModelProperty(value = " 炉体6#CO含量")
	private Float co6;
/**
 * 蒸汽压力
 */
	@ApiModelProperty(value = " 蒸汽压力")
	private Float steamPressure;
/**
 * 均热段炉膛压力
 */
	@ApiModelProperty(value = " 均热段炉膛压力")
	private Float avgPressure;
/**
 * 煤气总管压力
 */
	@ApiModelProperty(value = " 煤气总管压力")
	private Float gasPressure;
/**
 * 气泡液位1
 */
	@ApiModelProperty(value = " 气泡液位1")
	private Float bubble1;
/**
 * 气泡液位2
 */
	@ApiModelProperty(value = " 气泡液位2")
	private Float bubble2;
/**
 * 气泡压力
 */
	@ApiModelProperty(value = " 气泡压力")
	private Float avgBubble;
/**
 * 累计煤气
 */
	@ApiModelProperty(value = " 累计煤气")
	private Float sumGas;
/**
 * 煤气消耗
 */
	@ApiModelProperty(value = " 煤气消耗")
	private Float consumeGas;
/**
 * 空燃比
 */
	@ApiModelProperty(value = " 空燃比")
	private Float airNaturalRatio;

}
