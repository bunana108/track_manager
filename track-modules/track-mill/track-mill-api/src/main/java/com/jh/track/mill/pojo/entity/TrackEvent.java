package com.jh.track.mill.pojo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ：xy
 * @date ：Created in 2021/4/21 15:56
 * @description： 跟踪事件
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
public class TrackEvent implements Serializable {
    private String eventType ;
    private int eventId;
    private int value1;
    private int value2;
    private int value3;
    private float value4;
    private float value5;
    private float value6;
    private float value7;

}
