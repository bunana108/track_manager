package com.jh.track.mill.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jh.track.common.core.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
/**
 * 计划跟踪表
 * 
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@ApiModel("计划跟踪表的实体")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("track_pdi")
public class Pdi extends BaseEntity{
	private static final long serialVersionUID = 1L;
/**
 * 主键
 */
	@TableId(type = IdType.INPUT)
	@ApiModelProperty(value = " 主键")
	private Long id;
/**
 * 生产序号
 */
	@ApiModelProperty(value = " 生产序号")
	private Long rollingSeq;
/**
 * 轧制号
 */
	@ApiModelProperty(value = " 轧制号")
	private String lotNo;
/**
 * 顺序号
 */
	@ApiModelProperty(value = " 顺序号")
	private Integer serNo;
/**
 * 炉号
 */
	@ApiModelProperty(value = " 炉号")
	private String heatNo;
/**
 * 内控成分索引
 */
	@ApiModelProperty(value = " 内控成分索引")
	private String gkNo;
/**
 * 附加炉号(A:开坯,Z:正常
 */
	@ApiModelProperty(value = " 附加炉号(A:开坯,Z:正常")
	private String recode;
/**
 * 热装标志
 */
	@ApiModelProperty(value = " 热装标志")
	private String hotChange;
/**
 * 钢种
 */
	@ApiModelProperty(value = " 钢种")
	private String spec;
/**
 * 出钢记号
 */
	@ApiModelProperty(value = " 出钢记号")
	private String specMemo;
/**
 * 厚度
 */
	@ApiModelProperty(value = " 厚度")
	private Float thick;
/**
 * 宽度
 */
	@ApiModelProperty(value = " 宽度")
	private Float width;
/**
 * 长度
 */
	@ApiModelProperty(value = " 长度")
	private Float length;
/**
 * 内控码
 */
	@ApiModelProperty(value = " 内控码")
	private String indexNo;
/**
 * 储位
 */
	@ApiModelProperty(value = " 储位")
	private String location;
/**
 * 钢坯重量
 */
	@ApiModelProperty(value = " 钢坯重量")
	private Float billetWgt;
/**
 * 直径/厚度
 */
	@ApiModelProperty(value = " 直径/厚度")
	private Float orderCaliber;
/**
 * 热轧直径/厚度
 */
	@ApiModelProperty(value = " 热轧直径/厚度")
	private Float proCaliber;
/**
 * 成品长度
 */
	@ApiModelProperty(value = " 成品长度")
	private Float orderLength;
/**
 * 热轧长度
 */
	@ApiModelProperty(value = " 热轧长度")
	private Float proLength;
/**
 * 定尺要求
 */
	@ApiModelProperty(value = " 定尺要求")
	private String cusStd;
/**
 * 单倍尺长度
 */
	@ApiModelProperty(value = " 单倍尺长度")
	private Float singleLength;
/**
 * 包装支数
 */
	@ApiModelProperty(value = " 包装支数")
	private Float pcsOfBdl;
/**
 * 包装重量上限
 */
	@ApiModelProperty(value = " 包装重量上限")
	private Float wgtOfBdlMax;
/**
 * 包装重量下线
 */
	@ApiModelProperty(value = " 包装重量下线")
	private Float wgtOfBdlMin;
/**
 * 轧制要求(默认S)
 */
	@ApiModelProperty(value = " 轧制要求(默认S)")
	private String speciaFlag;
/**
 * 牌号
 */
	@ApiModelProperty(value = " 牌号")
	private String tradeNo;
/**
 * 产品形态码
 */
	@ApiModelProperty(value = " 产品形态码")
	private String prodTypeNo;
/**
 * 订单交货方式
 */
	@ApiModelProperty(value = " 订单交货方式")
	private String countWgtMode;
/**
 * 产品标准名称
 */
	@ApiModelProperty(value = " 产品标准名称")
	private String prodTypeChain;
/**
 * 特殊要求
 */
	@ApiModelProperty(value = " 特殊要求")
	private String wkmCode;
/**
 * 计划轧制日期
 */
	@ApiModelProperty(value = " 计划轧制日期")
	private String rollingDate;
/**
 * 日轧制顺序号
 */
	@ApiModelProperty(value = " 日轧制顺序号")
	private String seqNo;
/**
 * 加热一段温度最大值
 */
	@ApiModelProperty(value = " 加热一段温度最大值")
	private Integer heatTemp1Max;
/**
 * 加热一段温度目标值
 */
	@ApiModelProperty(value = " 加热一段温度目标值")
	private Integer heatTemp1Aim;
/**
 * 加热一段温度最小值
 */
	@ApiModelProperty(value = " 加热一段温度最小值")
	private Integer heatTemp1Min;
/**
 * 加热二段温度最大值
 */
	@ApiModelProperty(value = " 加热二段温度最大值")
	private Integer heatTemp2Max;
/**
 * 加热二段温度目标值
 */
	@ApiModelProperty(value = " 加热二段温度目标值")
	private Integer heatTemp2Aim;
/**
 * 加热二段温度最小值
 */
	@ApiModelProperty(value = " 加热二段温度最小值")
	private Integer heatTemp2Min;
/**
 * 均热段温度最大值
 */
	@ApiModelProperty(value = " 均热段温度最大值")
	private Integer avgTempMax;
/**
 * 均热段温度目标值
 */
	@ApiModelProperty(value = " 均热段温度目标值")
	private Integer avgTempAim;
/**
 * 均热段温度最小值
 */
	@ApiModelProperty(value = " 均热段温度最小值")
	private Integer avgTempMin;
/**
 * 开扎温度最大值
 */
	@ApiModelProperty(value = " 开扎温度最大值")
	private Integer slabTmpMax;
/**
 * 开扎温度目标值
 */
	@ApiModelProperty(value = " 开扎温度目标值")
	private Integer slabTmpAim;
/**
 * 开扎温度最小值
 */
	@ApiModelProperty(value = " 开扎温度最小值")
	private Integer slabTmpMin;
/**
 * 终扎温度最大值
 */
	@ApiModelProperty(value = " 终扎温度最大值")
	private Integer finshRollTmpMax;
/**
 * 终扎温度目标值
 */
	@ApiModelProperty(value = " 终扎温度目标值")
	private Integer finshRollTmpAim;
/**
 * 终扎温度最小值
 */
	@ApiModelProperty(value = " 终扎温度最小值")
	private Integer finshRollTmpMin;
/**
 * 上冷床温度最大值
 */
	@ApiModelProperty(value = " 上冷床温度最大值")
	private Integer onBedTmpMax;
/**
 * 上冷床温度目标值
 */
	@ApiModelProperty(value = " 上冷床温度目标值")
	private Integer onBedTmpAim;
/**
 * 上冷床温度最小值
 */
	@ApiModelProperty(value = " 上冷床温度最小值")
	private Integer onBedTmpMin;
/**
 * 总支数
 */
	@ApiModelProperty(value = " 总支数")
	private Integer totaBillet;
/**
 * 上线数量
 */
	@ApiModelProperty(value = " 上线数量")
	private Integer onlineCount;
/**
 * 在炉支数
 */
	@ApiModelProperty(value = " 在炉支数")
	private Integer inRhfCount;
/**
 * 出炉支数
 */
	@ApiModelProperty(value = " 出炉支数")
	private Integer dischargeCount;
/**
 * 平均重量
 */
	@ApiModelProperty(value = " 平均重量")
	private Float avgWgt;

}
