package com.jh.track.mill.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jh.track.common.core.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
/**
 * 设备跟踪表
 * 
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@ApiModel("设备跟踪表的实体")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("track_device")
public class Device extends BaseEntity{
	private static final long serialVersionUID = 1L;
/**
 * 主键
 */
	@TableId(type = IdType.INPUT)
	@ApiModelProperty(value = " 主键")
	private Long id;
/**
 * 设备编号
 */
	@ApiModelProperty(value = " 设备编号")
	private Integer deviceId;
/**
 * 设备名
 */
	@ApiModelProperty(value = " 设备名")
	private String deviceName;
/**
 * 使用状态
 */
	@ApiModelProperty(value = " 使用状态")
	private String useFlag;
/**
 * 设备区域
 */
	@ApiModelProperty(value = " 设备区域")
	private String deviceArea;

}
