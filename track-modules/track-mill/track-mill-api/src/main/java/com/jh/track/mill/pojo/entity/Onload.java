package com.jh.track.mill.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jh.track.common.core.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;
import java.io.Serializable;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
/**
 * 跟踪上料表
 * 
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@ApiModel("跟踪上料表的实体")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("track_onload")
public class Onload extends BaseEntity{
	private static final long serialVersionUID = 1L;
/**
 * 物料号
 */
	@TableId(type = IdType.INPUT)
	@ApiModelProperty(value = " 物料号")
	private Long matNo;
/**
 * 轧制序号
 */
	@ApiModelProperty(value = " 轧制序号")
	private String lotNo;
/**
 * 钢柸顺序号
 */
	@ApiModelProperty(value = " 钢柸顺序号")
	private String serialNumber;
/**
 * 炉号
 */
	@ApiModelProperty(value = " 炉号")
	private String heatNo;
/**
 * 钢坯号
 */
	@ApiModelProperty(value = " 钢坯号")
	private String billetId;
/**
 * 附加炉号
 */
	@ApiModelProperty(value = " 附加炉号")
	private String recode;
/**
 * 厚度
 */
	@ApiModelProperty(value = " 厚度")
	private Float thick;
/**
 * 宽度
 */
	@ApiModelProperty(value = " 宽度")
	private Float width;
/**
 * 内控码
 */
	@ApiModelProperty(value = " 内控码")
	private String indexNo;
/**
 * 长度
 */
	@ApiModelProperty(value = " 长度")
	private Float length;
/**
 * 储位
 */
	@ApiModelProperty(value = " 储位")
	private String location;
/**
 * 入炉状态
 */
	@ApiModelProperty(value = " 入炉状态")
	private Integer schpcs;
/**
 * 操作时间
 */
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = " 操作时间")
	private Date prodDate;
/**
 * 操作班别
 */
	@ApiModelProperty(value = " 操作班别")
	private String prodShift;
/**
 * 操作班次
 */
	@ApiModelProperty(value = " 操作班次")
	private String prodCrew;
/**
 * 操作人员
 */
	@ApiModelProperty(value = " 操作人员")
	private String prodEmpNo;
/**
 * 是否为尾批
 */
	@ApiModelProperty(value = " 是否为尾批")
	private String isLastNo;
/**
 * 加热炉编号
 */
	@ApiModelProperty(value = " 加热炉编号")
	private String reheat;
/**
 * 入炉时间
 */
	@JsonFormat(shape= JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = " 入炉时间")
	private Date inDate;
/**
 * 钢坯实测长度
 */
	@ApiModelProperty(value = " 钢坯实测长度")
	private Float measurEdlenght;
/**
 * 物料所在区域
 */
	@ApiModelProperty(value = " 物料所在区域")
	private Integer zoneId;
/**
 * 目标直径
 */
	@ApiModelProperty(value = " 目标直径")
	private Float orderDiameter;
/**
 * 牌号，钢种
 */
	@ApiModelProperty(value = " 牌号，钢种")
	private String tradeNo;
/**
 * 计划号
 */
	@ApiModelProperty(value = " 计划号")
	private Long pdiId;
/**
 * 平均中量
 */
	@ApiModelProperty(value = " 平均中量")
	private Float avgWight;
/**
 * 入炉支数
 */
	@ApiModelProperty(value = " 入炉支数")
	private Integer chargePcs;
/**
 * 是否热装
 */
	@ApiModelProperty(value = " 是否热装")
	private String hotStatus;
/**
 * 定尺长度
 */
	@ApiModelProperty(value = " 定尺长度")
	private BigDecimal fixedLength;

}
