package com.jh.track.mill.pojo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ：xy
 * @date ：Created in 2021/4/21 15:19
 * @description： PLC采集点我信息
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
public class Tag implements Serializable {
    private final static long serialVersionUID = 1L;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Value")
    private String value;
    @JsonProperty("Type")
    private String type;
    @JsonProperty("Qualitie")
    private String qualitie;
    @JsonProperty("MessageTime")
    private String messageTime;
}