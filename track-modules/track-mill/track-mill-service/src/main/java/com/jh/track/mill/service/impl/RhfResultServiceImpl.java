package com.jh.track.mill.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.utils.Query;

import com.jh.track.mill.mapper.RhfResultMapper;
import com.jh.track.mill.pojo.entity.RhfResult;
import com.jh.track.mill.service.RhfResultService;
import java.util.List;
/**
 * 业务接口实现
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */

@Service("rhfResultService")
public class RhfResultServiceImpl extends ServiceImpl<RhfResultMapper, RhfResult> implements RhfResultService {
    /**
     * 分页查询
     *
     * @return PageUtils
     */

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<RhfResult> page = this.page(
                new Query<RhfResult>().getPage(params),
                new QueryWrapper<RhfResult>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据id查询
     *
     * @param  id
     * @return RhfResult
     */
    @Override
    public RhfResult getRhfResultById(Long id) {
        RhfResult rhfResult =this.getById(id);
        return  rhfResult ;
    }

    /**
     * 查询列表
     *
     * @param params
     * @return RhfResult集合
     */
    @Override
    public List<RhfResult> getRhfResultList(Map<String, Object> params) {
        List<RhfResult> rhfResults = this.list();
        return rhfResults;
    }

    /**
     * 新增
     *
     * @param  rhfResult
     * @return boolean
     */
    @Override
    public boolean saveRhfResult(RhfResult rhfResult) {
        boolean status = this.save(rhfResult);
        return status;

    }

    /**
     * 修改
     *
     * @param$ rhfResult
     * @return boolean
     */
    @Override
    public boolean updateRhfResult(RhfResult rhfResult) {
        boolean status = this.updateById(rhfResult);
        return status;
    }

    /**
     * 批量删除
     *
     * @param  ids
     * @return boolean
     */
    @Override
    public boolean removeRhfResultByIds(List<Long> ids) {
        boolean status = this.removeByIds(ids);
        return status;
    }

    /**
     * 删除信息
     *
     * @param  id
     * @return boolean
     */
    @Override
    public boolean removeRhfResultById(Long id) {
        boolean status = this.removeById(id);
        return status;
    }
}