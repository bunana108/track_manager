package com.jh.track.mill.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jh.track.mill.pojo.entity.Event;
import com.jh.track.mill.service.EventService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.result.Result;
import com.jh.track.common.core.base.BaseController;


/**
 * 事件跟踪表控制器
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@RestController
@RequestMapping("/api.mill/v1/event")
@Api(value = "事件跟踪表", tags = "事件跟踪表接口")
public class EventController extends BaseController {
    @Autowired
    private EventService eventService;

    /**
    * 分页事件跟踪表列表}
    *
    * @param params 　
    * @return Result
    */
    @GetMapping("/page")
    @ApiOperation(value = "事件跟踪表列表", notes = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currPage", required = true, value = "当前页", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", required = true, value = "每页显示数据", paramType = "form"),
            @ApiImplicitParam(name = "totalPage", required = true, value = "总页数", paramType = "form"),
            @ApiImplicitParam(name = "totalCount", required = true, value = "总记录数", paramType = "form"),
    })

    public Result page(@RequestParam Map<String, Object> params){
        PageUtils page = eventService.queryPage(params);

        return Result.success(page);
    }
    /**
     * 所有事件跟踪表列表}
     *
     * @param params 　
     * @return Result
     */
    @GetMapping("/list")
    @ApiOperation(value = "事件跟踪表列表", notes = "查询所有")
    public Result list(@RequestParam Map<String, Object> params){
        List<Event>  events = eventService.getEventList(params);

        return Result.success(events);
    }


    /**
     * 事件跟踪表详情
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "事件跟踪表详情")
    @ApiImplicitParam(name = "{id", value = "{主键", required = true, paramType = "path", dataType = "Long")
    public Result detail(@PathVariable("id") Long id){
		Event event= eventService.getEventById(id);

        return Result.success(event);

    }

    /**
     * 事件跟踪表新增
     */
    @PostMapping
    @ApiOperation(value = "新增事件跟踪表")
    @ApiImplicitParam(name = "事件跟踪表", value = "实体JSON对象", required = true, paramType = "body", dataType = "Event")
    public Result add(@RequestBody Event event){
		boolean state = eventService.saveEvent(event);

        return Result.judge(state);
    }

    /**
     * 修改事件跟踪表
     */
    @PutMapping(value = "/{id}")
    @ApiOperation(value = "修改菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "事件跟踪表id}", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "event", value = "实体JSON对象", required = true, paramType = "body", dataType = "Event")
    })
    public Result update(@RequestBody Event event){
        boolean status = eventService.updateEvent(event);

        return Result.judge(status);
    }

    /**
     * 主键删除
     *
     * @param ids id字符串，根据,号分隔
     * @return Result
     */
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除事件跟踪表", notes = "删除事件跟踪表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "id集合字符串，以,分割", required = true, paramType = "query", dataType = "String")
    })
    public Result delete(@PathVariable("ids") String ids){
		boolean status =  eventService.removeEventByIds(Arrays.stream(ids.split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList()));

        return Result.judge(status);
    }

}
