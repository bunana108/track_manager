package com.jh.track.mill.common.constant;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 12:19
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public interface ESConstants {

    String LOGIN_INDEX_PREFIX = "track-auth-login-";


    String LOGIN_INDEX_PATTERN = "track-auth-login-*";

}
