package com.jh.track.mill.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jh.track.mill.pojo.entity.RhfResult;
import com.jh.track.mill.service.RhfResultService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.result.Result;
import com.jh.track.common.core.base.BaseController;


/**
 * 控制器
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@RestController
@RequestMapping("/api.mill/v1/rhfResult")
@Api(value = "", tags = "接口")
public class RhfResultController extends BaseController {
    @Autowired
    private RhfResultService rhfResultService;

    /**
    * 分页列表}
    *
    * @param params 　
    * @return Result
    */
    @GetMapping("/page")
    @ApiOperation(value = "列表", notes = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currPage", required = true, value = "当前页", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", required = true, value = "每页显示数据", paramType = "form"),
            @ApiImplicitParam(name = "totalPage", required = true, value = "总页数", paramType = "form"),
            @ApiImplicitParam(name = "totalCount", required = true, value = "总记录数", paramType = "form"),
    })

    public Result page(@RequestParam Map<String, Object> params){
        PageUtils page = rhfResultService.queryPage(params);

        return Result.success(page);
    }
    /**
     * 所有列表}
     *
     * @param params 　
     * @return Result
     */
    @GetMapping("/list")
    @ApiOperation(value = "列表", notes = "查询所有")
    public Result list(@RequestParam Map<String, Object> params){
        List<RhfResult>  rhfResults = rhfResultService.getRhfResultList(params);

        return Result.success(rhfResults);
    }


    /**
     * 详情
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "详情")
    @ApiImplicitParam(name = "{id", value = "{主键", required = true, paramType = "path", dataType = "Long")
    public Result detail(@PathVariable("id") Long id){
		RhfResult rhfResult= rhfResultService.getRhfResultById(id);

        return Result.success(rhfResult);

    }

    /**
     * 新增
     */
    @PostMapping
    @ApiOperation(value = "新增")
    @ApiImplicitParam(name = "", value = "实体JSON对象", required = true, paramType = "body", dataType = "RhfResult")
    public Result add(@RequestBody RhfResult rhfResult){
		boolean state = rhfResultService.saveRhfResult(rhfResult);

        return Result.judge(state);
    }

    /**
     * 修改
     */
    @PutMapping(value = "/{id}")
    @ApiOperation(value = "修改菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id}", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "rhfResult", value = "实体JSON对象", required = true, paramType = "body", dataType = "RhfResult")
    })
    public Result update(@RequestBody RhfResult rhfResult){
        boolean status = rhfResultService.updateRhfResult(rhfResult);

        return Result.judge(status);
    }

    /**
     * 主键删除
     *
     * @param ids id字符串，根据,号分隔
     * @return Result
     */
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除", notes = "删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "id集合字符串，以,分割", required = true, paramType = "query", dataType = "String")
    })
    public Result delete(@PathVariable("ids") String ids){
		boolean status =  rhfResultService.removeRhfResultByIds(Arrays.stream(ids.split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList()));

        return Result.judge(status);
    }

}
