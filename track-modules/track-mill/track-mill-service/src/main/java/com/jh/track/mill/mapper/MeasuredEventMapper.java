package com.jh.track.mill.mapper;

import com.jh.track.mill.pojo.entity.MeasuredEvent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * Mapper接口
 * 
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:13:49
 */
@Mapper
public interface MeasuredEventMapper extends BaseMapper<MeasuredEvent> {
	
}
