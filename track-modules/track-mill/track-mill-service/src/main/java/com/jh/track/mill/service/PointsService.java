package com.jh.track.mill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.mill.pojo.entity.Points;
import java.util.List;
import java.util.Map;

/**
 * 跟踪点位表业务接口
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
public interface PointsService extends IService<Points> {

    /**
     * 分页查询跟踪点位表
     *
     * @param params
     * @return PageUtils
     */
    PageUtils queryPage(Map<String, Object> params);


    /**
     * 根据id查询跟踪点位表
     *
     * @param  id
     * @return Points
     */
    public Points getPointsById(Long id);

    /**
     * 查询跟踪点位表列表
     *
     * @param  params
     * @return Points集合
     */
    public List<Points> getPointsList(Map<String, Object> params);

    /**
     * 新增跟踪点位表
     *
     * @param  points
     * @return boolean
     */
    public boolean savePoints(Points points);

    /**
     * 修改跟踪点位表
     *
     * @param  points
     * @return boolean
     */
    public boolean updatePoints(Points points);

    /**
     * 批量删除跟踪点位表
     *
     * @param  ids
     * @return boolean
     */
    public boolean removePointsByIds(List<Long> ids);

    /**
     * 删除跟踪点位表信息
     *
     * @param  id
     * @return boolean
     */
    public boolean removePointsById(Long id);

    public boolean refreshPointsCache();
}

