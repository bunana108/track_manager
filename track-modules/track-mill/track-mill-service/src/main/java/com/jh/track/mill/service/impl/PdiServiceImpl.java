package com.jh.track.mill.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.utils.Query;

import com.jh.track.mill.mapper.PdiMapper;
import com.jh.track.mill.pojo.entity.Pdi;
import com.jh.track.mill.service.PdiService;
import java.util.List;
/**
 * 计划跟踪表业务接口实现
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */

@Service("pdiService")
public class PdiServiceImpl extends ServiceImpl<PdiMapper, Pdi> implements PdiService {
    /**
     * 分页查询计划跟踪表
     *
     * @return PageUtils
     */

    @Override
    public PageUtils queryPage(Map<String, Object> params) {


        IPage<Pdi> page = this.page(
                new Query<Pdi>().getPage(params),
                new QueryWrapper<Pdi>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据id查询计划跟踪表
     *
     * @param  id
     * @return Pdi
     */
    @Override
    public Pdi getPdiById(Long id) {
        Pdi pdi =this.getById(id);
        return  pdi ;
    }

    /**
     * 查询计划跟踪表列表
     *
     * @param params
     * @return Pdi集合
     */
    @Override
    public List<Pdi> getPdiList(Map<String, Object> params) {
        List<Pdi> pdis = this.list();
        return pdis;
    }

    /**
     * 新增计划跟踪表
     *
     * @param  pdi
     * @return boolean
     */
    @Override
    public boolean savePdi(Pdi pdi) {
        boolean status = this.save(pdi);
        return status;

    }

    /**
     * 修改计划跟踪表
     *
     * @param$ pdi
     * @return boolean
     */
    @Override
    public boolean updatePdi(Pdi pdi) {
        boolean status = this.updateById(pdi);
        return status;
    }

    /**
     * 批量删除计划跟踪表
     *
     * @param  ids
     * @return boolean
     */
    @Override
    public boolean removePdiByIds(List<Long> ids) {
        boolean status = this.removeByIds(ids);
        return status;
    }

    /**
     * 删除计划跟踪表信息
     *
     * @param  id
     * @return boolean
     */
    @Override
    public boolean removePdiById(Long id) {
        boolean status = this.removeById(id);
        return status;
    }
}