package com.jh.track.mill.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.utils.Query;

import com.jh.track.mill.mapper.DeviceMapper;
import com.jh.track.mill.pojo.entity.Device;
import com.jh.track.mill.service.DeviceService;
import java.util.List;
/**
 * 设备跟踪表业务接口实现
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */

@Service("deviceService")
public class DeviceServiceImpl extends ServiceImpl<DeviceMapper, Device> implements DeviceService {
    /**
     * 分页查询设备跟踪表
     *
     * @return PageUtils
     */

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Device> page = this.page(
                new Query<Device>().getPage(params),
                new QueryWrapper<Device>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据id查询设备跟踪表
     *
     * @param  id
     * @return Device
     */
    @Override
    public Device getDeviceById(Long id) {
        Device device =this.getById(id);
        return  device ;
    }

    /**
     * 查询设备跟踪表列表
     *
     * @param params
     * @return Device集合
     */
    @Override
    public List<Device> getDeviceList(Map<String, Object> params) {
        List<Device> devices = this.list();
        return devices;
    }

    /**
     * 新增设备跟踪表
     *
     * @param  device
     * @return boolean
     */
    @Override
    public boolean saveDevice(Device device) {
        boolean status = this.save(device);
        return status;

    }

    /**
     * 修改设备跟踪表
     *
     * @param$ device
     * @return boolean
     */
    @Override
    public boolean updateDevice(Device device) {
        boolean status = this.updateById(device);
        return status;
    }

    /**
     * 批量删除设备跟踪表
     *
     * @param  ids
     * @return boolean
     */
    @Override
    public boolean removeDeviceByIds(List<Long> ids) {
        boolean status = this.removeByIds(ids);
        return status;
    }

    /**
     * 删除设备跟踪表信息
     *
     * @param  id
     * @return boolean
     */
    @Override
    public boolean removeDeviceById(Long id) {
        boolean status = this.removeById(id);
        return status;
    }
}