package com.jh.track.mill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

/**
 * @author ：xy
 * @date ：Created in 2021/4/21 0:06
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@SpringBootApplication
@EnableWebSocket
@EnableAsync
public class MillApplication {
    public static void main(String[] args) {
        SpringApplication.run(MillApplication.class,args);

    }
}
