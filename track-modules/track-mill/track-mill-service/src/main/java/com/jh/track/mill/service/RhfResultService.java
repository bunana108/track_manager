package com.jh.track.mill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.mill.pojo.entity.RhfResult;
import java.util.List;
import java.util.Map;

/**
 * 业务接口
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
public interface RhfResultService extends IService<RhfResult> {

    /**
     * 分页查询
     *
     * @param params
     * @return PageUtils
     */
    PageUtils queryPage(Map<String, Object> params);


    /**
     * 根据id查询
     *
     * @param  id
     * @return RhfResult
     */
    public RhfResult getRhfResultById(Long id);

    /**
     * 查询列表
     *
     * @param  params
     * @return RhfResult集合
     */
    public List<RhfResult> getRhfResultList(Map<String, Object> params);

    /**
     * 新增
     *
     * @param  rhfResult
     * @return boolean
     */
    public boolean saveRhfResult(RhfResult rhfResult);

    /**
     * 修改
     *
     * @param  rhfResult
     * @return boolean
     */
    public boolean updateRhfResult(RhfResult rhfResult);

    /**
     * 批量删除
     *
     * @param  ids
     * @return boolean
     */
    public boolean removeRhfResultByIds(List<Long> ids);

    /**
     * 删除信息
     *
     * @param  id
     * @return boolean
     */
    public boolean removeRhfResultById(Long id);
}

