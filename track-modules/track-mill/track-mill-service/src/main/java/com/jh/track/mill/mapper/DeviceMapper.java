package com.jh.track.mill.mapper;

import com.jh.track.mill.pojo.entity.Device;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 设备跟踪表Mapper接口
 * 
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@Mapper
public interface DeviceMapper extends BaseMapper<Device> {
	
}
