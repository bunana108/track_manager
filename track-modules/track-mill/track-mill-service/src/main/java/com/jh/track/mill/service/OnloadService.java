package com.jh.track.mill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.mill.pojo.entity.Onload;
import java.util.List;
import java.util.Map;

/**
 * 跟踪上料表业务接口
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
public interface OnloadService extends IService<Onload> {

    /**
     * 分页查询跟踪上料表
     *
     * @param params
     * @return PageUtils
     */
    PageUtils queryPage(Map<String, Object> params);


    /**
     * 根据id查询跟踪上料表
     *
     * @param  matNo
     * @return Onload
     */
    public Onload getOnloadById(Long matNo);

    /**
     * 查询跟踪上料表列表
     *
     * @param  params
     * @return Onload集合
     */
    public List<Onload> getOnloadList(Map<String, Object> params);

    /**
     * 新增跟踪上料表
     *
     * @param  onload
     * @return boolean
     */
    public boolean saveOnload(Onload onload);

    /**
     * 修改跟踪上料表
     *
     * @param  onload
     * @return boolean
     */
    public boolean updateOnload(Onload onload);

    /**
     * 批量删除跟踪上料表
     *
     * @param  matNos
     * @return boolean
     */
    public boolean removeOnloadByIds(List<Long> matNos);

    /**
     * 删除跟踪上料表信息
     *
     * @param  matNo
     * @return boolean
     */
    public boolean removeOnloadById(Long matNo);
}

