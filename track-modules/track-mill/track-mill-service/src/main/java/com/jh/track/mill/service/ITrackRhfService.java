package com.jh.track.mill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.mill.pojo.entity.TrackRhf;

import java.util.List;

/**
 * @author ：xy
 * @date ：Created in 2021/4/20 22:22
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public interface ITrackRhfService extends IService<TrackRhf> {

    TrackRhf findById(Long id);
    List<TrackRhf> findALlTrackRhf();
    void insertTrackRhf(TrackRhf trackRhf);
    void updateTrackRhf(TrackRhf trackRhf);
    void deleteTrackRhf(TrackRhf trackRhf);
}
