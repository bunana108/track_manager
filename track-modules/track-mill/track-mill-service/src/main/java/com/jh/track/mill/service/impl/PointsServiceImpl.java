package com.jh.track.mill.service.impl;

import com.jh.track.common.core.constant.MillEventConstants;
import com.jh.track.mill.pojo.entity.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.utils.Query;

import com.jh.track.mill.mapper.PointsMapper;
import com.jh.track.mill.pojo.entity.Points;
import com.jh.track.mill.service.PointsService;

/**
 * 跟踪点位表业务接口实现
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */

@Service("pointsService")
public class PointsServiceImpl extends ServiceImpl<PointsMapper, Points> implements PointsService {

    @Autowired
    private RedisTemplate redisTemplate;
    /**
     * 分页查询跟踪点位表
     *
     * @return PageUtils
     */

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Points> page = this.page(
                new Query<Points>().getPage(params),
                new QueryWrapper<Points>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据id查询跟踪点位表
     *
     * @param  id
     * @return Points
     */
    @Override
    public Points getPointsById(Long id) {
        Points points =this.getById(id);
        return  points ;
    }

    /**
     * 查询跟踪点位表列表
     *
     * @param params
     * @return Points集合
     */
    @Override
    public List<Points> getPointsList(Map<String, Object> params) {
        List<Points> pointss = this.list();
        return pointss;
    }

    /**
     * 新增跟踪点位表
     *
     * @param  points
     * @return boolean
     */
    @Override
    public boolean savePoints(Points points) {
        boolean status = this.save(points);
        return status;

    }

    /**
     * 修改跟踪点位表
     *
     * @param$ points
     * @return boolean
     */
    @Override
    public boolean updatePoints(Points points) {
        boolean status = this.updateById(points);
        return status;
    }

    /**
     * 批量删除跟踪点位表
     *
     * @param  ids
     * @return boolean
     */
    @Override
    public boolean removePointsByIds(List<Long> ids) {
        boolean status = this.removeByIds(ids);
        return status;
    }

    /**
     * 删除跟踪点位表信息
     *
     * @param  id
     * @return boolean
     */
    @Override
    public boolean removePointsById(Long id) {
        boolean status = this.removeById(id);
        return status;
    }

    @Override
    public boolean refreshPointsCache() {
        redisTemplate.delete(MillEventConstants.POINT_KEY);
        List<Points> points = this.getPointsList(null);
        Map<String, List<Points>> pointMap = new TreeMap<>();
        Map<Integer, List<Points>> pointMap_temp = Optional.ofNullable(points).orElse(new ArrayList<>()).stream().collect(Collectors.groupingBy(Points::getEventId));
        pointMap_temp.forEach((k,v)->{
            pointMap.put(k.toString(),v);
        });
        redisTemplate.opsForHash().putAll(MillEventConstants.POINT_KEY,pointMap);
        return  true;
    }
}