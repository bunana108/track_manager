package com.jh.track.mill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.mill.pojo.entity.Online;
import java.util.List;
import java.util.Map;

/**
 * 跟踪上线表业务接口
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
public interface OnlineService extends IService<Online> {

    /**
     * 分页查询跟踪上线表
     *
     * @param params
     * @return PageUtils
     */
    PageUtils queryPage(Map<String, Object> params);


    /**
     * 根据id查询跟踪上线表
     *
     * @param  matNo
     * @return Online
     */
    public Online getOnlineById(Long matNo);

    /**
     * 查询跟踪上线表列表
     *
     * @param  params
     * @return Online集合
     */
    public List<Online> getOnlineList(Map<String, Object> params);

    /**
     * 新增跟踪上线表
     *
     * @param  online
     * @return boolean
     */
    public boolean saveOnline(Online online);

    /**
     * 修改跟踪上线表
     *
     * @param  online
     * @return boolean
     */
    public boolean updateOnline(Online online);

    /**
     * 批量删除跟踪上线表
     *
     * @param  matNos
     * @return boolean
     */
    public boolean removeOnlineByIds(List<Long> matNos);

    /**
     * 删除跟踪上线表信息
     *
     * @param  matNo
     * @return boolean
     */
    public boolean removeOnlineById(Long matNo);
}

