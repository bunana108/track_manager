package com.jh.track.mill.compoent;


import com.jh.track.mill.service.EventService;
import com.jh.track.mill.service.ITrackRhfService;
import com.jh.track.mill.service.PointsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 容器启动完成时加载角色权限规则至Redis缓存
 * @author xy
 */
@Component
@AllArgsConstructor
@Slf4j
public class InitMillEvent implements CommandLineRunner {

    private ITrackRhfService iTrackRhfService;
    private EventService eventService;
    private PointsService pointsService;

    @Override
    public void run(String... args) {
        eventService.refreshEventsCache();
        pointsService.refreshPointsCache();


    }
}
