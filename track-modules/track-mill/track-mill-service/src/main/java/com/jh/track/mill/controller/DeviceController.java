package com.jh.track.mill.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jh.track.mill.pojo.entity.Device;
import com.jh.track.mill.service.DeviceService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.result.Result;
import com.jh.track.common.core.base.BaseController;


/**
 * 设备跟踪表控制器
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@RestController
@RequestMapping("/api.mill/v1/device")
@Api(value = "设备跟踪表", tags = "设备跟踪表接口")
public class DeviceController extends BaseController {
    @Autowired
    private DeviceService deviceService;

    /**
    * 分页设备跟踪表列表}
    *
    * @param params 　
    * @return Result
    */
    @GetMapping("/page")
    @ApiOperation(value = "设备跟踪表列表", notes = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currPage", required = true, value = "当前页", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", required = true, value = "每页显示数据", paramType = "form"),
            @ApiImplicitParam(name = "totalPage", required = true, value = "总页数", paramType = "form"),
            @ApiImplicitParam(name = "totalCount", required = true, value = "总记录数", paramType = "form"),
    })

    public Result page(@RequestParam Map<String, Object> params){
        PageUtils page = deviceService.queryPage(params);

        return Result.success(page);
    }
    /**
     * 所有设备跟踪表列表}
     *
     * @param params 　
     * @return Result
     */
    @GetMapping("/list")
    @ApiOperation(value = "设备跟踪表列表", notes = "查询所有")
    public Result list(@RequestParam Map<String, Object> params){
        List<Device>  devices = deviceService.getDeviceList(params);

        return Result.success(devices);
    }


    /**
     * 设备跟踪表详情
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "设备跟踪表详情")
    @ApiImplicitParam(name = "{id", value = "{主键", required = true, paramType = "path", dataType = "Long")
    public Result detail(@PathVariable("id") Long id){
		Device device= deviceService.getDeviceById(id);

        return Result.success(device);

    }

    /**
     * 设备跟踪表新增
     */
    @PostMapping
    @ApiOperation(value = "新增设备跟踪表")
    @ApiImplicitParam(name = "设备跟踪表", value = "实体JSON对象", required = true, paramType = "body", dataType = "Device")
    public Result add(@RequestBody Device device){
		boolean state = deviceService.saveDevice(device);

        return Result.judge(state);
    }

    /**
     * 修改设备跟踪表
     */
    @PutMapping(value = "/{id}")
    @ApiOperation(value = "修改菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "设备跟踪表id}", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "device", value = "实体JSON对象", required = true, paramType = "body", dataType = "Device")
    })
    public Result update(@RequestBody Device device){
        boolean status = deviceService.updateDevice(device);

        return Result.judge(status);
    }

    /**
     * 主键删除
     *
     * @param ids id字符串，根据,号分隔
     * @return Result
     */
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除设备跟踪表", notes = "删除设备跟踪表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "id集合字符串，以,分割", required = true, paramType = "query", dataType = "String")
    })
    public Result delete(@PathVariable("ids") String ids){
		boolean status =  deviceService.removeDeviceByIds(Arrays.stream(ids.split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList()));

        return Result.judge(status);
    }

}
