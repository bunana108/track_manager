package com.jh.track.mill.service.impl;

import com.jh.track.common.core.constant.MillEventConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.utils.Query;

import com.jh.track.mill.mapper.EventMapper;
import com.jh.track.mill.pojo.entity.Event;
import com.jh.track.mill.service.EventService;

/**
 * 事件跟踪表业务接口实现
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */

@Service("eventService")
public class EventServiceImpl extends ServiceImpl<EventMapper, Event> implements EventService {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 分页查询事件跟踪表
     *
     * @return PageUtils
     */

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Event> page = this.page(
                new Query<Event>().getPage(params),
                new QueryWrapper<Event>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据id查询事件跟踪表
     *
     * @param  id
     * @return Event
     */
    @Override
    public Event getEventById(Long id) {
        Event event =this.getById(id);
        return  event ;
    }

    /**
     * 查询事件跟踪表列表
     *
     * @param params
     * @return Event集合
     */
    @Override
    public List<Event> getEventList(Map<String, Object> params) {
        List<Event> events = this.list();
        return events;
    }

    /**
     * 新增事件跟踪表
     *
     * @param  event
     * @return boolean
     */
    @Override
    public boolean saveEvent(Event event) {
        boolean status = this.save(event);
        return status;

    }

    /**
     * 修改事件跟踪表
     *
     * @param$ event
     * @return boolean
     */
    @Override
    public boolean updateEvent(Event event) {
        boolean status = this.updateById(event);
        return status;
    }

    /**
     * 批量删除事件跟踪表
     *
     * @param  ids
     * @return boolean
     */
    @Override
    public boolean removeEventByIds(List<Long> ids) {
        boolean status = this.removeByIds(ids);
        return status;
    }

    /**
     * 删除事件跟踪表信息
     *
     * @param  id
     * @return boolean
     */
    @Override
    public boolean removeEventById(Long id) {
        boolean status = this.removeById(id);
        return status;
    }

    @Override
    public boolean refreshEventsCache() {
        redisTemplate.delete(MillEventConstants.EVENTS_KEY);
        List<Event> events = this.getEventList(null);
        Map<String, Event> eventMap = new TreeMap<>();
        Optional.ofNullable(events).orElse(new ArrayList<>()).forEach(event->{
            eventMap.put(event.getEventId().toString(),event);
        });
        redisTemplate.opsForHash().putAll(MillEventConstants.EVENTS_KEY,eventMap);
        return  true;
    }
}