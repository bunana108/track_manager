package com.jh.track.mill.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.utils.Query;

import com.jh.track.mill.mapper.MeasuredEventMapper;
import com.jh.track.mill.pojo.entity.MeasuredEvent;
import com.jh.track.mill.service.MeasuredEventService;
import java.util.List;
/**
 * 业务接口实现
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:13:49
 */

@Service("measuredEventService")
public class MeasuredEventServiceImpl extends ServiceImpl<MeasuredEventMapper, MeasuredEvent> implements MeasuredEventService {
    /**
     * 分页查询
     *
     * @return PageUtils
     */

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MeasuredEvent> page = this.page(
                new Query<MeasuredEvent>().getPage(params),
                new QueryWrapper<MeasuredEvent>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据id查询
     *
     * @param  id
     * @return MeasuredEvent
     */
    @Override
    public MeasuredEvent getMeasuredEventById(Long id) {
        MeasuredEvent measuredEvent =this.getById(id);
        return  measuredEvent ;
    }

    /**
     * 查询列表
     *
     * @param params
     * @return MeasuredEvent集合
     */
    @Override
    public List<MeasuredEvent> getMeasuredEventList(Map<String, Object> params) {
        List<MeasuredEvent> measuredEvents = this.list();
        return measuredEvents;
    }

    /**
     * 新增
     *
     * @param  measuredEvent
     * @return boolean
     */
    @Override
    public boolean saveMeasuredEvent(MeasuredEvent measuredEvent) {
        boolean status = this.save(measuredEvent);
        return status;

    }

    /**
     * 修改
     *
     * @param$ measuredEvent
     * @return boolean
     */
    @Override
    public boolean updateMeasuredEvent(MeasuredEvent measuredEvent) {
        boolean status = this.updateById(measuredEvent);
        return status;
    }

    /**
     * 批量删除
     *
     * @param  ids
     * @return boolean
     */
    @Override
    public boolean removeMeasuredEventByIds(List<Long> ids) {
        boolean status = this.removeByIds(ids);
        return status;
    }

    /**
     * 删除信息
     *
     * @param  id
     * @return boolean
     */
    @Override
    public boolean removeMeasuredEventById(Long id) {
        boolean status = this.removeById(id);
        return status;
    }
}