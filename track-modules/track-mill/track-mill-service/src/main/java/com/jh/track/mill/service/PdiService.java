package com.jh.track.mill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.mill.pojo.entity.Pdi;
import java.util.List;
import java.util.Map;

/**
 * 计划跟踪表业务接口
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
public interface PdiService extends IService<Pdi> {

    /**
     * 分页查询计划跟踪表
     *
     * @param params
     * @return PageUtils
     */
    PageUtils queryPage(Map<String, Object> params);


    /**
     * 根据id查询计划跟踪表
     *
     * @param  id
     * @return Pdi
     */
    public Pdi getPdiById(Long id);

    /**
     * 查询计划跟踪表列表
     *
     * @param  params
     * @return Pdi集合
     */
    public List<Pdi> getPdiList(Map<String, Object> params);

    /**
     * 新增计划跟踪表
     *
     * @param  pdi
     * @return boolean
     */
    public boolean savePdi(Pdi pdi);

    /**
     * 修改计划跟踪表
     *
     * @param  pdi
     * @return boolean
     */
    public boolean updatePdi(Pdi pdi);

    /**
     * 批量删除计划跟踪表
     *
     * @param  ids
     * @return boolean
     */
    public boolean removePdiByIds(List<Long> ids);

    /**
     * 删除计划跟踪表信息
     *
     * @param  id
     * @return boolean
     */
    public boolean removePdiById(Long id);
}

