package com.jh.track.mill.config;

import com.jh.track.mill.pojo.entity.Event;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.AbstractQueue;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author ：xy
 * @date ：Created in 2021/4/22 15:01
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Configuration
public class QueueConfig {

    @Bean
    public ConcurrentLinkedQueue<Event> concurrentLinkedQueue(){
        return new ConcurrentLinkedQueue<Event>();
    }

}
