package com.jh.track.mill.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jh.track.mill.pojo.entity.Rhf;
import com.jh.track.mill.service.RhfService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.result.Result;
import com.jh.track.common.core.base.BaseController;


/**
 * 加热炉跟踪表控制器
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@RestController
@RequestMapping("/api.mill/v1/rhf")
@Api(value = "加热炉跟踪表", tags = "加热炉跟踪表接口")
public class RhfController extends BaseController {
    @Autowired
    private RhfService rhfService;

    /**
    * 分页加热炉跟踪表列表}
    *
    * @param params 　
    * @return Result
    */
    @GetMapping("/page")
    @ApiOperation(value = "加热炉跟踪表列表", notes = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currPage", required = true, value = "当前页", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", required = true, value = "每页显示数据", paramType = "form"),
            @ApiImplicitParam(name = "totalPage", required = true, value = "总页数", paramType = "form"),
            @ApiImplicitParam(name = "totalCount", required = true, value = "总记录数", paramType = "form"),
    })

    public Result page(@RequestParam Map<String, Object> params){
        PageUtils page = rhfService.queryPage(params);

        return Result.success(page);
    }
    /**
     * 所有加热炉跟踪表列表}
     *
     * @param params 　
     * @return Result
     */
    @GetMapping("/list")
    @ApiOperation(value = "加热炉跟踪表列表", notes = "查询所有")
    public Result list(@RequestParam Map<String, Object> params){
        List<Rhf>  rhfs = rhfService.getRhfList(params);

        return Result.success(rhfs);
    }


    /**
     * 加热炉跟踪表详情
     */
    @GetMapping("/{matNo}")
    @ApiOperation(value = "加热炉跟踪表详情")
    @ApiImplicitParam(name = "{matNo", value = "{物料号", required = true, paramType = "path", dataType = "Long")
    public Result detail(@PathVariable("matNo") Long matNo){
		Rhf rhf= rhfService.getRhfById(matNo);

        return Result.success(rhf);

    }

    /**
     * 加热炉跟踪表新增
     */
    @PostMapping
    @ApiOperation(value = "新增加热炉跟踪表")
    @ApiImplicitParam(name = "加热炉跟踪表", value = "实体JSON对象", required = true, paramType = "body", dataType = "Rhf")
    public Result add(@RequestBody Rhf rhf){
		boolean state = rhfService.saveRhf(rhf);

        return Result.judge(state);
    }

    /**
     * 修改加热炉跟踪表
     */
    @PutMapping(value = "/{id}")
    @ApiOperation(value = "修改菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "matNo", value = "加热炉跟踪表id}", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "rhf", value = "实体JSON对象", required = true, paramType = "body", dataType = "Rhf")
    })
    public Result update(@RequestBody Rhf rhf){
        boolean status = rhfService.updateRhf(rhf);

        return Result.judge(status);
    }

    /**
     * 物料号删除
     *
     * @param matNos id字符串，根据,号分隔
     * @return Result
     */
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除加热炉跟踪表", notes = "删除加热炉跟踪表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "id集合字符串，以,分割", required = true, paramType = "query", dataType = "String")
    })
    public Result delete(@PathVariable("ids") String ids){
		boolean status =  rhfService.removeRhfByIds(Arrays.stream(ids.split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList()));

        return Result.judge(status);
    }

}
