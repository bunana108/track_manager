package com.jh.track.mill.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.utils.Query;

import com.jh.track.mill.mapper.RhfMapper;
import com.jh.track.mill.pojo.entity.Rhf;
import com.jh.track.mill.service.RhfService;
import java.util.List;
/**
 * 加热炉跟踪表业务接口实现
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */

@Service("rhfService")
public class RhfServiceImpl extends ServiceImpl<RhfMapper, Rhf> implements RhfService {
    /**
     * 分页查询加热炉跟踪表
     *
     * @return PageUtils
     */

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Rhf> page = this.page(
                new Query<Rhf>().getPage(params),
                new QueryWrapper<Rhf>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据id查询加热炉跟踪表
     *
     * @param  matNo
     * @return Rhf
     */
    @Override
    public Rhf getRhfById(Long matNo) {
        Rhf rhf =this.getById(matNo);
        return  rhf ;
    }

    /**
     * 查询加热炉跟踪表列表
     *
     * @param params
     * @return Rhf集合
     */
    @Override
    public List<Rhf> getRhfList(Map<String, Object> params) {
        List<Rhf> rhfs = this.list();
        return rhfs;
    }

    /**
     * 新增加热炉跟踪表
     *
     * @param  rhf
     * @return boolean
     */
    @Override
    public boolean saveRhf(Rhf rhf) {
        boolean status = this.save(rhf);
        return status;

    }

    /**
     * 修改加热炉跟踪表
     *
     * @param$ rhf
     * @return boolean
     */
    @Override
    public boolean updateRhf(Rhf rhf) {
        boolean status = this.updateById(rhf);
        return status;
    }

    /**
     * 批量删除加热炉跟踪表
     *
     * @param  matNos
     * @return boolean
     */
    @Override
    public boolean removeRhfByIds(List<Long> matNos) {
        boolean status = this.removeByIds(matNos);
        return status;
    }

    /**
     * 删除加热炉跟踪表信息
     *
     * @param  matNo
     * @return boolean
     */
    @Override
    public boolean removeRhfById(Long matNo) {
        boolean status = this.removeById(matNo);
        return status;
    }
}