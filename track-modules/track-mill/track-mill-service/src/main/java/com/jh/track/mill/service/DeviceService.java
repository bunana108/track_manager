package com.jh.track.mill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.mill.pojo.entity.Device;
import java.util.List;
import java.util.Map;

/**
 * 设备跟踪表业务接口
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
public interface DeviceService extends IService<Device> {

    /**
     * 分页查询设备跟踪表
     *
     * @param params
     * @return PageUtils
     */
    PageUtils queryPage(Map<String, Object> params);


    /**
     * 根据id查询设备跟踪表
     *
     * @param  id
     * @return Device
     */
    public Device getDeviceById(Long id);

    /**
     * 查询设备跟踪表列表
     *
     * @param  params
     * @return Device集合
     */
    public List<Device> getDeviceList(Map<String, Object> params);

    /**
     * 新增设备跟踪表
     *
     * @param  device
     * @return boolean
     */
    public boolean saveDevice(Device device);

    /**
     * 修改设备跟踪表
     *
     * @param  device
     * @return boolean
     */
    public boolean updateDevice(Device device);

    /**
     * 批量删除设备跟踪表
     *
     * @param  ids
     * @return boolean
     */
    public boolean removeDeviceByIds(List<Long> ids);

    /**
     * 删除设备跟踪表信息
     *
     * @param  id
     * @return boolean
     */
    public boolean removeDeviceById(Long id);
}

