package com.jh.track.mill.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.utils.Query;

import com.jh.track.mill.mapper.OnlineMapper;
import com.jh.track.mill.pojo.entity.Online;
import com.jh.track.mill.service.OnlineService;
import java.util.List;
/**
 * 跟踪上线表业务接口实现
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */

@Service("onlineService")
public class OnlineServiceImpl extends ServiceImpl<OnlineMapper, Online> implements OnlineService {
    /**
     * 分页查询跟踪上线表
     *
     * @return PageUtils
     */

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Online> page = this.page(
                new Query<Online>().getPage(params),
                new QueryWrapper<Online>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据id查询跟踪上线表
     *
     * @param  matNo
     * @return Online
     */
    @Override
    public Online getOnlineById(Long matNo) {
        Online online =this.getById(matNo);
        return  online ;
    }

    /**
     * 查询跟踪上线表列表
     *
     * @param params
     * @return Online集合
     */
    @Override
    public List<Online> getOnlineList(Map<String, Object> params) {
        List<Online> onlines = this.list();
        return onlines;
    }

    /**
     * 新增跟踪上线表
     *
     * @param  online
     * @return boolean
     */
    @Override
    public boolean saveOnline(Online online) {
        boolean status = this.save(online);
        return status;

    }

    /**
     * 修改跟踪上线表
     *
     * @param$ online
     * @return boolean
     */
    @Override
    public boolean updateOnline(Online online) {
        boolean status = this.updateById(online);
        return status;
    }

    /**
     * 批量删除跟踪上线表
     *
     * @param  matNos
     * @return boolean
     */
    @Override
    public boolean removeOnlineByIds(List<Long> matNos) {
        boolean status = this.removeByIds(matNos);
        return status;
    }

    /**
     * 删除跟踪上线表信息
     *
     * @param  matNo
     * @return boolean
     */
    @Override
    public boolean removeOnlineById(Long matNo) {
        boolean status = this.removeById(matNo);
        return status;
    }
}