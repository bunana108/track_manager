package com.jh.track.mill.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jh.track.mill.pojo.entity.Points;
import com.jh.track.mill.service.PointsService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.result.Result;
import com.jh.track.common.core.base.BaseController;


/**
 * 跟踪点位表控制器
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@RestController
@RequestMapping("/api.mill/v1/points")
@Api(value = "跟踪点位表", tags = "跟踪点位表接口")
public class PointsController extends BaseController {
    @Autowired
    private PointsService pointsService;

    /**
    * 分页跟踪点位表列表}
    *
    * @param params 　
    * @return Result
    */
    @GetMapping("/page")
    @ApiOperation(value = "跟踪点位表列表", notes = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currPage", required = true, value = "当前页", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", required = true, value = "每页显示数据", paramType = "form"),
            @ApiImplicitParam(name = "totalPage", required = true, value = "总页数", paramType = "form"),
            @ApiImplicitParam(name = "totalCount", required = true, value = "总记录数", paramType = "form"),
    })

    public Result page(@RequestParam Map<String, Object> params){
        PageUtils page = pointsService.queryPage(params);

        return Result.success(page);
    }
    /**
     * 所有跟踪点位表列表}
     *
     * @param params 　
     * @return Result
     */
    @GetMapping("/list")
    @ApiOperation(value = "跟踪点位表列表", notes = "查询所有")
    public Result list(@RequestParam Map<String, Object> params){
        List<Points>  pointss = pointsService.getPointsList(params);

        return Result.success(pointss);
    }


    /**
     * 跟踪点位表详情
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "跟踪点位表详情")
    @ApiImplicitParam(name = "{id", value = "{主键", required = true, paramType = "path", dataType = "Long")
    public Result detail(@PathVariable("id") Long id){
		Points points= pointsService.getPointsById(id);

        return Result.success(points);

    }

    /**
     * 跟踪点位表新增
     */
    @PostMapping
    @ApiOperation(value = "新增跟踪点位表")
    @ApiImplicitParam(name = "跟踪点位表", value = "实体JSON对象", required = true, paramType = "body", dataType = "Points")
    public Result add(@RequestBody Points points){
		boolean state = pointsService.savePoints(points);

        return Result.judge(state);
    }

    /**
     * 修改跟踪点位表
     */
    @PutMapping(value = "/{id}")
    @ApiOperation(value = "修改菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "跟踪点位表id}", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "points", value = "实体JSON对象", required = true, paramType = "body", dataType = "Points")
    })
    public Result update(@RequestBody Points points){
        boolean status = pointsService.updatePoints(points);

        return Result.judge(status);
    }

    /**
     * 主键删除
     *
     * @param ids id字符串，根据,号分隔
     * @return Result
     */
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除跟踪点位表", notes = "删除跟踪点位表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "id集合字符串，以,分割", required = true, paramType = "query", dataType = "String")
    })
    public Result delete(@PathVariable("ids") String ids){
		boolean status =  pointsService.removePointsByIds(Arrays.stream(ids.split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList()));

        return Result.judge(status);
    }

}
