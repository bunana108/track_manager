package com.jh.track.mill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jh.track.mill.pojo.entity.TrackRhf;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author ：xy
 * @date ：Created in 2021/4/20 22:21
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Mapper
public interface TrackRhfMapper extends BaseMapper<TrackRhf> {
}
