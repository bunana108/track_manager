package com.jh.track.mill.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.utils.Query;

import com.jh.track.mill.mapper.OnloadMapper;
import com.jh.track.mill.pojo.entity.Onload;
import com.jh.track.mill.service.OnloadService;
import java.util.List;
/**
 * 跟踪上料表业务接口实现
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */

@Service("onloadService")
public class OnloadServiceImpl extends ServiceImpl<OnloadMapper, Onload> implements OnloadService {
    /**
     * 分页查询跟踪上料表
     *
     * @return PageUtils
     */

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<Onload> page = this.page(
                new Query<Onload>().getPage(params),
                new QueryWrapper<Onload>()
        );

        return new PageUtils(page);
    }

    /**
     * 根据id查询跟踪上料表
     *
     * @param  matNo
     * @return Onload
     */
    @Override
    public Onload getOnloadById(Long matNo) {
        Onload onload =this.getById(matNo);
        return  onload ;
    }

    /**
     * 查询跟踪上料表列表
     *
     * @param params
     * @return Onload集合
     */
    @Override
    public List<Onload> getOnloadList(Map<String, Object> params) {
        List<Onload> onloads = this.list();
        return onloads;
    }

    /**
     * 新增跟踪上料表
     *
     * @param  onload
     * @return boolean
     */
    @Override
    public boolean saveOnload(Onload onload) {
        boolean status = this.save(onload);
        return status;

    }

    /**
     * 修改跟踪上料表
     *
     * @param$ onload
     * @return boolean
     */
    @Override
    public boolean updateOnload(Onload onload) {
        boolean status = this.updateById(onload);
        return status;
    }

    /**
     * 批量删除跟踪上料表
     *
     * @param  matNos
     * @return boolean
     */
    @Override
    public boolean removeOnloadByIds(List<Long> matNos) {
        boolean status = this.removeByIds(matNos);
        return status;
    }

    /**
     * 删除跟踪上料表信息
     *
     * @param  matNo
     * @return boolean
     */
    @Override
    public boolean removeOnloadById(Long matNo) {
        boolean status = this.removeById(matNo);
        return status;
    }
}