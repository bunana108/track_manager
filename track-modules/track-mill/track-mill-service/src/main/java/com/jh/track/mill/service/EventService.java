package com.jh.track.mill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.mill.pojo.entity.Event;
import java.util.List;
import java.util.Map;

/**
 * 事件跟踪表业务接口
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
public interface EventService extends IService<Event> {

    /**
     * 分页查询事件跟踪表
     *
     * @param params
     * @return PageUtils
     */
    PageUtils queryPage(Map<String, Object> params);


    /**
     * 根据id查询事件跟踪表
     *
     * @param  id
     * @return Event
     */
    public Event getEventById(Long id);

    /**
     * 查询事件跟踪表列表
     *
     * @param  params
     * @return Event集合
     */
    public List<Event> getEventList(Map<String, Object> params);

    /**
     * 新增事件跟踪表
     *
     * @param  event
     * @return boolean
     */
    public boolean saveEvent(Event event);

    /**
     * 修改事件跟踪表
     *
     * @param  event
     * @return boolean
     */
    public boolean updateEvent(Event event);

    /**
     * 批量删除事件跟踪表
     *
     * @param  ids
     * @return boolean
     */
    public boolean removeEventByIds(List<Long> ids);

    /**
     * 删除事件跟踪表信息
     *
     * @param  id
     * @return boolean
     */
    public boolean removeEventById(Long id);


    /**
     * 刷新redis中的事件
     * @author xy
     * @date 2021/5/5 23:43
     * @return boolean
     */
    public boolean refreshEventsCache();
}

