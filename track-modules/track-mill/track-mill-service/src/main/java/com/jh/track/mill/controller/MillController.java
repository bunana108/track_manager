package com.jh.track.mill.controller;

import com.jh.track.common.core.result.Result;
import com.jh.track.mill.pojo.entity.TrackRhf;
import com.jh.track.mill.service.ITrackRhfService;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：xy
 * @date ：Created in 2021/4/20 22:20
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */

@Api(tags = "加热炉跟踪接口")
@RestController
@RequestMapping("/api.track/v1/rhf")
@Slf4j
@AllArgsConstructor
public class MillController {
    @Autowired
    private ITrackRhfService iTrackRhfService;
    @GetMapping("/{id}")
    public Result get(@PathVariable("id") Long id){
        TrackRhf trackRhf = iTrackRhfService.findById(id);
        return Result.success(trackRhf);

    }

}
