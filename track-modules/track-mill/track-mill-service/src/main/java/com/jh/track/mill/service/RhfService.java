package com.jh.track.mill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.mill.pojo.entity.Rhf;
import java.util.List;
import java.util.Map;

/**
 * 加热炉跟踪表业务接口
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
public interface RhfService extends IService<Rhf> {

    /**
     * 分页查询加热炉跟踪表
     *
     * @param params
     * @return PageUtils
     */
    PageUtils queryPage(Map<String, Object> params);


    /**
     * 根据id查询加热炉跟踪表
     *
     * @param  matNo
     * @return Rhf
     */
    public Rhf getRhfById(Long matNo);

    /**
     * 查询加热炉跟踪表列表
     *
     * @param  params
     * @return Rhf集合
     */
    public List<Rhf> getRhfList(Map<String, Object> params);

    /**
     * 新增加热炉跟踪表
     *
     * @param  rhf
     * @return boolean
     */
    public boolean saveRhf(Rhf rhf);

    /**
     * 修改加热炉跟踪表
     *
     * @param  rhf
     * @return boolean
     */
    public boolean updateRhf(Rhf rhf);

    /**
     * 批量删除加热炉跟踪表
     *
     * @param  matNos
     * @return boolean
     */
    public boolean removeRhfByIds(List<Long> matNos);

    /**
     * 删除加热炉跟踪表信息
     *
     * @param  matNo
     * @return boolean
     */
    public boolean removeRhfById(Long matNo);
}

