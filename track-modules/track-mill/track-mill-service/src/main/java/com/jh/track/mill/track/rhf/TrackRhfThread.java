package com.jh.track.mill.track.rhf;

import com.jh.track.mill.pojo.entity.Event;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.PostConstruct;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;

/**
 * @author ：xy
 * @date ：Created in 2021/4/22 15:25
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Slf4j
public class TrackRhfThread  {
    @Autowired
    @Qualifier(value = "crawlExecutorPool")
    private ExecutorService pool;

    @Autowired
    private ConcurrentLinkedQueue<Event> concurrentLinkedQueue;
    //容器启动完成开始执行跟踪
    @PostConstruct
    public void trackThread(){
        pool.execute(() -> {
                while (true){
                    if (!concurrentLinkedQueue.isEmpty()){
                        //执行跟踪方法
                        Event event = concurrentLinkedQueue.poll();
                        switch (event.getEventId()){
                            case 10001:
                                log.info("入炉!!!!!!!");
                                break;
                            case 10002:
                                log.info("出炉!!!!!!!");
                                break;
                            case 10003:
                                log.info("步进梁前进!!!!!!!");
                                break;
                            case 10004:
                                log.info("步进梁后退!!!!!!!");
                                break;
                            default:
                                break;
                        }
                        //通过webSocket通知前台数据刷新

                    }
                }



        });
    }

}
