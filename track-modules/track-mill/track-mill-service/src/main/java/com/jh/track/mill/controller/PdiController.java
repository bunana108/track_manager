package com.jh.track.mill.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.jh.track.mill.pojo.entity.Pdi;
import com.jh.track.mill.service.PdiService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.common.core.result.Result;
import com.jh.track.common.core.base.BaseController;


/**
 * 计划跟踪表控制器
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:17:35
 */
@RestController
@RequestMapping("/api.mill/v1/pdi")
@Api(value = "计划跟踪表", tags = "计划跟踪表接口")
public class PdiController extends BaseController {
    @Autowired
    private PdiService pdiService;

    /**
    * 分页计划跟踪表列表}
    *
    * @param params 　
    * @return Result
    */
    @GetMapping("/page")
    @ApiOperation(value = "计划跟踪表列表", notes = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "currPage", required = true, value = "当前页", paramType = "form"),
            @ApiImplicitParam(name = "pageSize", required = true, value = "每页显示数据", paramType = "form"),
            @ApiImplicitParam(name = "totalPage", required = true, value = "总页数", paramType = "form"),
            @ApiImplicitParam(name = "totalCount", required = true, value = "总记录数", paramType = "form"),
    })

    public Result page(@RequestParam Map<String, Object> params){
        PageUtils page = pdiService.queryPage(params);

        return Result.success(page);
    }
    /**
     * 所有计划跟踪表列表}
     *
     * @param params 　
     * @return Result
     */
    @GetMapping("/list")
    @ApiOperation(value = "计划跟踪表列表", notes = "查询所有")
    public Result list(@RequestParam Map<String, Object> params){
        List<Pdi>  pdis = pdiService.getPdiList(params);

        return Result.success(pdis);
    }


    /**
     * 计划跟踪表详情
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "计划跟踪表详情")
    @ApiImplicitParam(name = "id", value = "主键", required = true, paramType = "path", dataType = "Long")
    public Result detail(@PathVariable("id") Long id){
		Pdi pdi= pdiService.getPdiById(id);

        return Result.success(pdi);

    }

    /**
     * 计划跟踪表新增
     */
    @PostMapping
    @ApiOperation(value = "新增计划跟踪表")
    @ApiImplicitParam(name = "计划跟踪表", value = "实体JSON对象", required = true, paramType = "body", dataType = "Pdi")
    public Result add(@RequestBody Pdi pdi){
		boolean state = pdiService.savePdi(pdi);

        return Result.judge(state);
    }

    /**
     * 修改计划跟踪表
     */
    @PutMapping(value = "/{id}")
    @ApiOperation(value = "修改菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "计划跟踪表id}", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "pdi", value = "实体JSON对象", required = true, paramType = "body", dataType = "Pdi")
    })
    public Result update(@RequestBody Pdi pdi){
        boolean status = pdiService.updatePdi(pdi);

        return Result.judge(status);
    }

    /**
     * 主键删除
     *
     * @param ids id字符串，根据,号分隔
     * @return Result
     */
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除计划跟踪表", notes = "删除计划跟踪表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "id集合字符串，以,分割", required = true, paramType = "query", dataType = "String")
    })
    public Result delete(@PathVariable("ids") String ids){
		boolean status =  pdiService.removePdiByIds(Arrays.stream(ids.split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList()));

        return Result.judge(status);
    }

}
