package com.jh.track.mill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.common.core.utils.PageUtils;
import com.jh.track.mill.pojo.entity.MeasuredEvent;
import java.util.List;
import java.util.Map;

/**
 * 业务接口
 *
 * @author xy
 * @email 924760866@qq.com
 * @date 2021-04-29 14:13:49
 */
public interface MeasuredEventService extends IService<MeasuredEvent> {

    /**
     * 分页查询
     *
     * @param params
     * @return PageUtils
     */
    PageUtils queryPage(Map<String, Object> params);


    /**
     * 根据id查询
     *
     * @param  id
     * @return MeasuredEvent
     */
    public MeasuredEvent getMeasuredEventById(Long id);

    /**
     * 查询列表
     *
     * @param  params
     * @return MeasuredEvent集合
     */
    public List<MeasuredEvent> getMeasuredEventList(Map<String, Object> params);

    /**
     * 新增
     *
     * @param  measuredEvent
     * @return boolean
     */
    public boolean saveMeasuredEvent(MeasuredEvent measuredEvent);

    /**
     * 修改
     *
     * @param  measuredEvent
     * @return boolean
     */
    public boolean updateMeasuredEvent(MeasuredEvent measuredEvent);

    /**
     * 批量删除
     *
     * @param  ids
     * @return boolean
     */
    public boolean removeMeasuredEventByIds(List<Long> ids);

    /**
     * 删除信息
     *
     * @param  id
     * @return boolean
     */
    public boolean removeMeasuredEventById(Long id);
}

