package com.jh.track.admin;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author ：xy
 * @date ：Created in 2021/4/19 22:06
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AdminApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class AdminTest {
}
