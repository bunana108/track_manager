package com.jh.track.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.jh.track.admin.pojo.entity.OauthClientDetails;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OauthClientDetailsMapper  extends BaseMapper<OauthClientDetails> {
}
