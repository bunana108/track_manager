package com.jh.track.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.jh.track.admin.pojo.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
