package com.jh.track.admin.common.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 12:19
 * @description：权限类型
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public enum PermTypeEnum {

    ROUTE(1, "路由权限"),
    BUTTON(2, "按钮权限");

    @Getter
    @Setter
    private Integer value;

    PermTypeEnum(Integer value, String desc) {
        this.value = value;
    }
}