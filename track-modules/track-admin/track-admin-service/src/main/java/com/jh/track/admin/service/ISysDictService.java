package com.jh.track.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.admin.pojo.entity.SysDict;


public interface ISysDictService extends IService<SysDict> {
}
