package com.jh.track.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.jh.track.admin.mapper.SysDictMapper;
import com.jh.track.admin.pojo.entity.SysDict;
import com.jh.track.admin.service.ISysDictService;
import org.springframework.stereotype.Service;

@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

}
