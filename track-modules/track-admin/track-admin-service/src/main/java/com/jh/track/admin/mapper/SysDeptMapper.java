package com.jh.track.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.jh.track.admin.pojo.entity.SysDept;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
