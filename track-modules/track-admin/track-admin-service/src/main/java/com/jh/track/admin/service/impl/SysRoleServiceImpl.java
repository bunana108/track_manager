package com.jh.track.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.github.pagehelper.Page;
import com.jh.track.admin.mapper.SysRoleMapper;
import com.jh.track.admin.pojo.entity.SysRole;
import com.jh.track.admin.pojo.entity.SysRoleMenu;
import com.jh.track.admin.pojo.entity.SysRolePermission;
import com.jh.track.admin.pojo.entity.SysUserRole;
import com.jh.track.admin.service.ISysRoleMenuService;
import com.jh.track.admin.service.ISysRolePermissionService;
import com.jh.track.admin.service.ISysRoleService;
import com.jh.track.admin.service.ISysUserRoleService;
import com.jh.track.common.web.exception.BizException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    private ISysRoleMenuService iSysRoleMenuService;
    private ISysUserRoleService iSysUserRoleService;
    private ISysRolePermissionService iSysRolePermissionService;


    @Override
    public boolean delete(List<Long> ids) {
        Optional.ofNullable(ids).orElse(new ArrayList<>()).forEach(id -> {
            int count = iSysUserRoleService.count(new LambdaQueryWrapper<SysUserRole>().eq(SysUserRole::getRoleId, id));
            if (count > 0) {
                throw new BizException("该角色已分配用户，无法删除");
            }
            iSysRoleMenuService.remove(new LambdaQueryWrapper<SysRoleMenu>().eq(SysRoleMenu::getRoleId, id));
            iSysRolePermissionService.remove(new LambdaQueryWrapper<SysRolePermission>().eq(SysRolePermission::getRoleId,id));
        });
        return this.removeByIds(ids);
    }


}
