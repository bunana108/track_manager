package com.jh.track.admin;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 17:40
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableDubbo
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class);
    }
}

