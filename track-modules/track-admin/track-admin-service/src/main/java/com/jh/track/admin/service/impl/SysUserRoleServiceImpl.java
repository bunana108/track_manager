package com.jh.track.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.jh.track.admin.mapper.SysUserRoleMapper;
import com.jh.track.admin.pojo.entity.SysUserRole;
import com.jh.track.admin.service.ISysUserRoleService;
import org.springframework.stereotype.Service;

@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
