package com.jh.track.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.admin.pojo.entity.OauthClientDetails;


public interface IOauthClientDetailsService extends IService<OauthClientDetails> {
}
