package com.jh.track.admin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.jh.track.admin.pojo.entity.SysUserRole;


public interface ISysUserRoleService extends IService<SysUserRole> {
}
