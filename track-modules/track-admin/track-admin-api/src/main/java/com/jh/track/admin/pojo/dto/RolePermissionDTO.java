package com.jh.track.admin.pojo.dto;
import com.jh.track.common.core.base.BaseDTO;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class RolePermissionDTO extends BaseDTO {
    private Long roleId;
    private List<Long> permissionIds;
    private Integer type;

    private Long moduleId;
}
