package com.jh.track.admin.pojo.dto;

import com.jh.track.common.core.base.BaseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class UserDTO extends BaseDTO {

    private Long id;
    private String username;
    private String password;
    private Integer status;
    private String clientId;
    private List<Long> roleIds;

}
