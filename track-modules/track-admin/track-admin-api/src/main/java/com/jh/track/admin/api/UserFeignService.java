package com.jh.track.admin.api;

import com.jh.track.admin.pojo.dto.UserDTO;
import com.jh.track.common.core.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 11:44
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@FeignClient(value = "track-admin")
public interface UserFeignService {
    @GetMapping("/api.admin/v1/users/username/{username}")
    Result<UserDTO> getUserByUsername(@PathVariable String username);
}
