package com.jh.track.admin.api.rpc;

import com.jh.track.admin.pojo.dto.UserDTO;

import com.jh.track.common.core.result.Result;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author ：xy
 * @date ：Created in 2021/4/22 23:34
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public interface UserService {
     Result<UserDTO> getUserByUsername(@PathVariable String username);
}
