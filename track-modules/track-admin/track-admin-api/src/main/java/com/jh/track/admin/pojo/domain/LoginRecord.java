package com.jh.track.admin.pojo.domain;

import com.jh.track.common.core.base.BaseDocument;

import lombok.Data;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 11:44
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
public class LoginRecord extends BaseDocument {

    private String clientIP;

    private long elapsedTime;

    private Object message;

    private String token;

    private String username;

    private String loginTime;

    private String region;

    /**
     * 会话状态 0-离线 1-在线
     */
    private Integer status;

}
