package com.jh.track.common.rabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:56
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Configuration
@EnableTransactionManagement
@Slf4j
public class RabbitMqConfig {

//    @Autowired
//    private RabbitTemplate rabbitTemplate;

    /**
     * 使用json序列化机制，进行消息转换
     *
     * @return
     */
    @Bean
    public MessageConverter jackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }




}
