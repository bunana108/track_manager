package com.jh.track.common.core.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:50
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public enum BusinessTypeEnum {

    USER("user", 100),
    MEMBER("member", 200),
    ORDER("order", 300);

    @Getter
    @Setter
    private String code;

    @Getter
    @Setter
    private Integer value;

    BusinessTypeEnum(String code, Integer value) {
        this.code = code;
        this.value = value;
    }

    public static BusinessTypeEnum getValue(String code) {
        for (BusinessTypeEnum value : values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return null;
    }
}
