package com.jh.track.common.core.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:46
 * @description：础分页请求对象
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
public class BasePageQuery {

    @ApiModelProperty(value = "当前页", example = "1")
    private int pageNum = 1;

    @ApiModelProperty(value = "每页记录数", example = "10")
    private int pageSize = 10;
}
