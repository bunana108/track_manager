package com.jh.track.common.core.base;

import lombok.Data;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:46
 * @description： document 是 ES 里的一个 JSON 对象，包括零个或多个field，类比关系数据库的一行记录
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
public class BaseDocument {
    /**
     * 数据唯一标识
     */
    private String id;

    /**
     * 索引名称
     */
    private String index;
}
