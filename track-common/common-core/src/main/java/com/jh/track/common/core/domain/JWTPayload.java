package com.jh.track.common.core.domain;

import lombok.Data;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:50
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
public class JWTPayload {

    private String jti;

    private Long exp;
}
