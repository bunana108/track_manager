package com.jh.track.common.core.base;

import java.io.Serializable;

/**
 * @author ：xy
 * @date ：Created in 2021/4/23 9:33
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public class BaseDTO implements Serializable {
    private static final long serialVersionUID = 1L;
}
