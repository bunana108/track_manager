package com.jh.track.common.core.constant;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:49
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public interface GlobalConstants {
    Integer STATUS_NORMAL_VALUE = 1;

    Integer VISIBLE_SHOW_VALUE = 1;

    String DEFAULT_USER_PASSWORD = "123456";

    Integer DELETED_VALUE = 1;

}
