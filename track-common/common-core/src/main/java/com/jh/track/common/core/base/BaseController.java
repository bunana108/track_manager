package com.jh.track.common.core.base;

import com.jh.track.common.core.utils.DateUtil;
import com.jh.track.common.core.utils.DateUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.Date;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:46
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public class BaseController {
    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date 类型转换
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(DateUtil.parseLocalDateTime(text, DateUtil.DATETIME_FORMATTER));
            }
        });
    }
}

