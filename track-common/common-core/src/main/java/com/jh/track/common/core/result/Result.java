package com.jh.track.common.core.result;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jh.track.common.core.constant.TrackConstant;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:50
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
@Getter
@ApiModel(value = "统一响应消息报文")
public class Result<T> implements Serializable {
    @ApiModelProperty(value = "状态码", required = true)
    private String code;

    @ApiModelProperty(value = "业务数据")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;
    @ApiModelProperty(value = "时间戳", required = true)
    private long time;

    @ApiModelProperty(value = "消息内容", required = true)
    private String msg;

    public static <T> Result<T> success() {
        return success(null);
    }

    public static <T> Result<T> success(T data) {
        ResultCode rce = ResultCode.SUCCESS;
        if (data instanceof Boolean && Boolean.FALSE.equals(data)) {
            rce = ResultCode.SYSTEM_EXECUTION_ERROR;
        }
        return result(rce, data);
    }
    private Result() {
        this.time = System.currentTimeMillis();
    }
    private Result(String resultCode, String msg) {
        this(resultCode, null, msg);
    }
    public static <T> Result<T> success(T data, Long total) {
        Result<T> result = new Result();
        result.setCode(ResultCode.SUCCESS.getCode());
        result.setMsg(ResultCode.SUCCESS.getMsg());
        result.setData(data);

        return result;
    }

    public static <T> Result<T> failed() {
        return result(ResultCode.SYSTEM_EXECUTION_ERROR.getCode(), ResultCode.SYSTEM_EXECUTION_ERROR.getMsg(), null);
    }

    public static <T> Result<T> failed(String msg) {
        return result(ResultCode.SYSTEM_EXECUTION_ERROR.getCode(), msg, null);
    }

    public static <T> Result<T> judge(boolean status) {
        if (status) {
            return success();
        } else {
            return failed();
        }
    }

    public static <T> Result<T> failed(IResultCode resultCode) {
        return result(resultCode.getCode(), resultCode.getMsg(), null);
    }

    private static <T> Result<T> result(IResultCode resultCode, T data) {
        return result(resultCode.getCode(), resultCode.getMsg(), data);
    }

    private Result(String code, T data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
        this.time = System.currentTimeMillis();
    }
    private static <T> Result<T> result(String code, String msg, T data) {
        Result<T> result = new Result<T>(code, data, msg);
        result.setData(data);
        return result;
    }


    public static boolean isSuccess(Result result) {
        if(result!=null&&ResultCode.SUCCESS.getCode().equals(result.getCode())){
            return true;
        }
        return false;
    }
    public static <T> Result<T> data(T data) {
        return data(data, ResultCode.SUCCESS.getMsg());
    }


    public static <T> Result<T> data(T data, String msg) {
        return data(ResultCode.SUCCESS.getCode(), data, msg);
    }
    public static <T> Result<T> data(String code, T data, String msg) {
        return new Result<>(code, data, data == null ? TrackConstant.DEFAULT_NULL_MESSAGE : msg);
    }
    public static <T> Result<T> condition(boolean flag) {
        return flag ? success(TrackConstant.DEFAULT_SUCCESS_MESSAGE) : fail(TrackConstant.DEFAULT_FAIL_MESSAGE);
    }
    public static <T> Result<T> success(String msg) {
        return new Result<>(ResultCode.SUCCESS.getCode(), msg);
    }
    private static <T> Result<T> fail(String msg) {
        return new Result<>(ResultCode.FAILURE.getCode(), msg);
    }
}
