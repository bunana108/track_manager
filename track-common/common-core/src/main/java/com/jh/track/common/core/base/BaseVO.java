package com.jh.track.common.core.base;

import java.io.Serializable;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:47
 * @description：VO 基类
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public class BaseVO implements Serializable {
    private static final long serialVersionUID = 1L;
}
