package com.jh.track.common.core.constant;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:49
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public interface RedisConstants {
    String BUSINESS_NO_PREFIX = "business_no:";
}
