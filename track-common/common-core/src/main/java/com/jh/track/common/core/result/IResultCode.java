package com.jh.track.common.core.result;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:50
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public interface IResultCode {

    String getCode();

    String getMsg();

}
