package com.jh.track.common.core.constant;

/**
 * @author ：xy
 * @date ：Created in 2021/5/5 23:47
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public interface MillEventConstants {
    String  EVENTS_KEY = "mill:events";
    String POINT_KEY = "mill:points";

}
