package com.jh.track.common.elasticsearch.constant;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 11:08
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
public interface ESConstants {
    int DEFAULT_PAGE_SIZE = 10;
}
