package com.jh.track.common.web.exception;

import com.jh.track.common.core.result.IResultCode;
import lombok.Getter;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 11:15
 * @description： 自定义业务异常
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Getter
public class BizException extends RuntimeException {

    public IResultCode resultCode;

    public BizException(IResultCode errorCode) {
        super(errorCode.getMsg());
        this.resultCode = errorCode;
    }

    public BizException(String message){
        super(message);
    }

    public BizException(String message, Throwable cause){
        super(message, cause);
    }

    public BizException(Throwable cause){
        super(cause);
    }
}