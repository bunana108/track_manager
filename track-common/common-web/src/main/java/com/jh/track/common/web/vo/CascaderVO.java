package com.jh.track.common.web.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 11:13
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Data
@Accessors(chain = true)
public class CascaderVO {

    private String value;

    private String label;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<CascaderVO> children;
}
