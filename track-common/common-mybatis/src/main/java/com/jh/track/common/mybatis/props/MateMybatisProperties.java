

package com.jh.track.common.mybatis.props;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 日志打印配置
 *
 * @author L.cm pangu
 */
@Getter
@Setter
@RefreshScope
@ConfigurationProperties("track.mybatis")
public class MateMybatisProperties {
	/**
	 * 是否打印可执行 sql
	 */
	private boolean sql = true;
}
