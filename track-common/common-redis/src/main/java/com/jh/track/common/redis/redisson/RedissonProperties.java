package com.jh.track.common.redis.redisson;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:58
 * @description： Redisson 连接配置配置
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Component
@ConfigurationProperties(prefix = "redisson")
@Data
public class RedissonProperties {

    private String serverAddress;

    private String port;

    private String password;

}
