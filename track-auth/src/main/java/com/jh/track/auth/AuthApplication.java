package com.jh.track.auth;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.jh.track.admin.api.UserFeignService;

import org.apache.dubbo.config.spring.context.annotation.DubboComponentScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author ：xy
 * @date ：Created in 2021/4/17 12:43
 * @description：
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
//@EnableFeignClients(basePackageClasses = {UserFeignService.class})
@SpringBootApplication
@EnableDiscoveryClient
@DubboComponentScan(basePackages = "com.jh.track")
public class AuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class);
    }
}
