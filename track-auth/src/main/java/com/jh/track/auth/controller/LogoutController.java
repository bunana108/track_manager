package com.jh.track.auth.controller;

import cn.hutool.json.JSONObject;
import com.jh.track.common.core.constant.AuthConstants;
import com.jh.track.common.core.result.Result;
import com.jh.track.common.web.util.RequestUtils;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;
/**
 * @author ：xy
 * @date ：Created in 2021/4/17 10:49
 * @description：注销ontroller
 * @version: v 1.0.0
 * @email: xy100826@163.com
 */
@Api(tags = "注销")
@RestController
@RequestMapping("/oauth")
@AllArgsConstructor
@Slf4j
public class LogoutController {

    private RedisTemplate redisTemplate;

    @DeleteMapping("/logout")
    public Result logout() {
        JSONObject jsonObject = RequestUtils.getJwtPayload();
        String jti = jsonObject.getStr(AuthConstants.JWT_JTI); // JWT唯一标识
        long exp = jsonObject.getLong(AuthConstants.JWT_EXP); // JWT过期时间戳

        long currentTimeSeconds = System.currentTimeMillis() / 1000;

        if (exp < currentTimeSeconds) { // token已过期，无需加入黑名单
            return Result.success();
        }
        redisTemplate.opsForValue().set(AuthConstants.TOKEN_BLACKLIST_PREFIX + jti, null, (exp - currentTimeSeconds), TimeUnit.SECONDS);
        return Result.success();
    }

}
